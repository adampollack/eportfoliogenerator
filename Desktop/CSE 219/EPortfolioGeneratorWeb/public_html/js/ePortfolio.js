/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// CONSTANTS
var IMG_PATH;
var ICON_PATH;
var IMG_WIDTH;
var SCALED_IMAGE_HEIGHT;
var SCALED_VIDEO_HEIGHT;
var FADE_TIME;
var SLIDESHOW_SLEEP_TIME;
var VID_PATH;
var IMG_TAG;
var VID_TAG;
var P_TAG;
var TEXT_TAG;
var PATH_TAG;
var LIST_TAG;
var SLIDE_TAG;

// DATA FOR CURRENT SLIDE
var title;
var slides;
var currentSlide;

// TIMER FOR PLAYING SLIDESHOW
var timer;

function Slide(initImgFile, initCaption) {
    this.imgFile = initImgFile;
    this.caption = initCaption;
}

function initPage() {
    IMG_PATH = "./img/";
    ICON_PATH = "./icons/";
    VID_PATH = "./vid/";
    IMG_WIDTH = 1000;
    SCALED_IMAGE_HEIGHT = 500.0;
    SCALED_VIDEO_HEIGHT = 500;
    FADE_TIME = 1000;
    SLIDESHOW_SLEEP_TIME = 3000;
    IMG_TAG = "image_file_name";
    VID_TAG = "video_file_name";
    P_TAG = "paragraph";
    TEXT_TAG = "text";
    PATH_TAG = "path";
    LIST_TAG = "list";
    SLIDE_TAG = "slides";
    var pageDataFile = "./data/site.json";
    slides = new Array();
    timer = null;
    loadData(pageDataFile);
    
}

function initSlides() {
    if (currentSlide >= 0) {
	$("#slide_caption").html(slides[currentSlide].caption);
	$("#slide_img").attr("src", IMG_PATH + slides[currentSlide].imgFile);
	$("#slide_img").one("load", function() {
	    autoScaleImage('#slide_img');
	});
    }
}

function autoScaleVideo(id) {
	var origHeight = $(id).height();
	var scaleFactor = SCALED_VIDEO_HEIGHT/origHeight;
	var origWidth = $(id).width();
	var scaledWidth = origWidth * scaleFactor;
	$(id).height(SCALED_VIDEO_HEIGHT);
	$(id).width(scaledWidth);
	var left = (IMG_WIDTH-scaledWidth)/2;
	$(id).css("left", left);
}

function autoScaleImage(id) {
	var origHeight = $(id).height();
	var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
	var origWidth = $(id).width();
	var scaledWidth = origWidth * scaleFactor;
	$(id).height(SCALED_IMAGE_HEIGHT);
	$(id).width(scaledWidth);
	var left = (IMG_WIDTH-scaledWidth)/2;
	$(id).css("left", left);
}

function autoScaleBannerImage(id){
    var origHeight = $(id).height();
	var scaleFactor = 80/origHeight;
	var origWidth = $(id).width();
	var scaledWidth = origWidth * scaleFactor;
	$(id).height(80);
	$(id).width(scaledWidth);
	var left = (80-scaledWidth)/2;
	$(id).css("left", left);
}

function fadeInCurrentSlide() {
    var filePath = IMG_PATH + slides[currentSlide].imgFile;
    $("#slide_img").fadeOut(FADE_TIME, function(){
	$(this).attr("src", filePath).bind('onreadystatechange load', function(){
	    if (this.complete) {
		$(this).fadeIn(FADE_TIME);
		$("#slide_caption").html(slides[currentSlide].caption);
		autoScaleImage();
	    }
	});
    });     
}

function loadData(jsonFile) {
    $.getJSON(jsonFile, function(json) {
	loadPage(json);
    });
}

function loadPage(pageData) {
    //Load and Display Banner
    var studentName = pageData.Header.student_name;
    var div1 = $("<div id='bannername'>");
    div1.appendTo('#banner');
    div1.css("display","inline-block");
    $("<h1>"+studentName+"</h1>").appendTo("#bannername");
    
    //Load and Display Banner Image
    var bannerImage = pageData.Header.image_file_name;
    var img = $('<img id="bannerimg">');
    img.attr('src', IMG_PATH + bannerImage);
    img.appendTo('#banner');
    autoScaleBannerImage("#bannerimg");
    
    
    //Load NavBar
    /*
     * Note for future implementation: can iterate through the area and have 
     * element with name specification (i.e. "page_name":"Home") to use as name
     * for hyperlink. Also have a url/path for reference
     */
    var homePage = "<a href="+"./index.html>"+"Home"+"</a>";
    $(homePage).appendTo('#navbar');
    
    var coursePage = "<a href="+"./courses.html>"+"Courses"+"</a>";
    $(coursePage).appendTo('#navbar');
    
    var coolPage = "<a href="+"./cool_websites.html>"+"Cool Websites"+"</a>";
    $(coolPage).appendTo('#navbar');
    
    var bestPage = "<a href="+"./best_vacation.html>"+"Best Vacation"+"</a>";
    $(bestPage).appendTo('#navbar');
    
    var aboutPage = "<a href="+"./about_me.html>"+"About Me"+"</a>";
    $(aboutPage).appendTo('#navbar');
    
    //Load Components
    var href = document.location.href;
    var docName = href.substring(href.lastIndexOf('/') + 1, href.lastIndexOf('.'));
    var pages = pageData.pages;
    
    for (var i = 0; i < pages.length; i++) {
        var name = Object.keys(pages[i]).toString();
        if(name === docName){
            var component = pages[i][name];
            for(var j = 0; j < component.length; j++){
                    var inner_name = Object.keys(component[j]).toString();
                    if(inner_name.indexOf(',') > -1)
                        inner_name = inner_name.substring(0, inner_name.indexOf(","));
                    switch(inner_name){
                        case IMG_TAG:
                            var div = $("<div>");
                            div.appendTo('#components');
                            div.css("text-align","center");
                             var image = component[j].image_file_name;
                             var imge = $('<img id="bodyimg">');
                             imge.attr('src', IMG_PATH + image);
                             imge.appendTo(div);
                             autoScaleImage('#bodyimg');
                             var caption = component[j].caption;
                             var cap = $('<p id="caption">' + caption + "<br>" + "</p>");
                             cap.appendTo('#components');
                            break;
                            
                        case VID_TAG:
                            var viddiv = $("<div>");
                            viddiv.appendTo('#components');
                            viddiv.css("text-align","center");
                            var vid = $("<video id='bodyvid' controls>");
                            var vide = component[j].video_file_name;
                            vid.attr('src', VID_PATH + vide);
                            vid.attr('type', "video/mp4");
                            vid.appendTo(viddiv);
                
                            break;
                            
                        case P_TAG:
                            var par = component[j].paragraph;
                            var parg = $('<p id="bodyp">' + par + '</p>');
                            parg.appendTo('#components');
                            
                            break;
                            
                        case TEXT_TAG:
                            
                            var link = component[j].text;
                            var path = component[j].path;
                            var a = "<a href=" +path+ ">" + link+ "</a>";
                            $(a).appendTo('#components');
                            
                            break;
                            
                         case LIST_TAG:
                             var ul = $("<ul id='lists'>");
                             var lists = component[j].list;
                             for(var m = 0; m < lists.length; m++){
                                var li = $("<li><p></p></li>");
                                $("p",li).text(lists[m].li);
                                 $(li).appendTo(ul);
                             }
                             $(ul).appendTo("#components");
                             
                             break;
                             
                        case SLIDE_TAG:
                            var slids = $('<div id="slide_show">');
                            var imgs = $('<img id="slide_img">');
                            var caps = $('<div id="slide_caption">');
                            var controls = $('<div id="slideshow_controls">');
                            var prev = $('<input id="previous_button" type="image" src="./icons/Previous.png" onclick="processPreviousRequest()">');
                            var play = $('<input id="play_pause_button" type="image" src="./icons/Play.png" onclick="processPlayPauseRequest()">');
                            var next = $('<input id="next_button" type="image" src="./icons/Next.png" onclick="processNextRequest()">');
                            
                            $(slids).appendTo("#components");
                            $(controls).appendTo("#components");
                            
                            
                            
                            $(imgs).appendTo(slids);
                            $(caps).appendTo(slids);
                            $(prev).appendTo(controls);
                            $(play).appendTo(controls);
                            $(next).appendTo(controls);
                            
                            loadSlideshow(component[j].slides);
                            initSlides();
                            
                            
                            
                            break;
                            
                    case "header":
                        var header = component[j].header;
                        var head = $('<h3 id="bodyh">' + header + '</p>');
                        $(head).appendTo('#components');
                    }
                    
                        
                        
                }
            }
        
    }
}

function loadSlideshow(slidesArray) {
    for (var i = 0; i < slidesArray.length; i++) {
	var rawSlide = slidesArray[i];
	var slide = new Slide(rawSlide.image_file_name, rawSlide.caption);
	slides[i] = slide;
    }
    if (slides.length > 0)
	currentSlide = 0;
    else
	currentSlide = -1;
}

function processPreviousRequest() {
    currentSlide--;
    if (currentSlide < 0)
	currentSlide = slides.length-1;
    fadeInCurrentSlide();
}

function processPlayPauseRequest() {
    if (timer === null) {
	timer = setInterval(processNextRequest, SLIDESHOW_SLEEP_TIME);
	$("#play_pause_button").attr("src", ICON_PATH + "Pause.png");
    }
    else {
	clearInterval(timer);
	timer = null;
	$("#play_pause_button").attr("src", ICON_PATH + "Play.png");
    }	
}

function processNextRequest() {
    currentSlide++;
    if (currentSlide >= slides.length)
	currentSlide = 0;
    fadeInCurrentSlide();
}
