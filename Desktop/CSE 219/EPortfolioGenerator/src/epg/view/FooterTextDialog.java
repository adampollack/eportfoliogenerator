/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class FooterTextDialog extends Stage{
    
    public FooterTextDialog(){
        this.setTitle("Add Footer Text");
        
        TextArea paragraph = new TextArea();
        paragraph.setWrapText(true);
        paragraph.setEditable(true);
        
        Label paragraphLabel = new Label("Enter Your Text:");
        
        Button submit = new Button("SUBMIT");
	
	GridPane grid = new GridPane();
	grid.add(paragraphLabel, 0, 0);
	grid.add(paragraph, 0, 1);
        grid.add(submit, 0, 2);
        
        submit.setOnAction(e -> {
            this.hide();
        });
        
        paragraphLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene paragraphDialog = new Scene(grid);
        
        paragraphDialog.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(paragraphDialog);
        
        paragraph.setScrollTop(0.0);
        
        
    
    }
}
