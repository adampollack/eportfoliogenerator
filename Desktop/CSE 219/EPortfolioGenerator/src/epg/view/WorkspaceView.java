/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import com.sun.glass.ui.Window;
import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.DEFAULT_PAGE_TITLE;
import static epg.LanguagePropertyType.LABEL_EPORTFOLIO_TITLE;
import static epg.LanguagePropertyType.LABEL_PAGE_TITLE;
import static epg.LanguagePropertyType.TOOLTIP_ADD_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_BANNER_IMAGE;
import static epg.LanguagePropertyType.TOOLTIP_COLORS;
import static epg.LanguagePropertyType.TOOLTIP_EXIT;
import static epg.LanguagePropertyType.TOOLTIP_EXPORT_EPORTFOLIO;
import static epg.LanguagePropertyType.TOOLTIP_FONTS;
import static epg.LanguagePropertyType.TOOLTIP_EDIT_HYPERLINK;
import static epg.LanguagePropertyType.TOOLTIP_HYPERLINK;
import static epg.LanguagePropertyType.TOOLTIP_IMAGE;
import static epg.LanguagePropertyType.TOOLTIP_LAYOUTS;
import static epg.LanguagePropertyType.TOOLTIP_LOAD_EPORTFOLIO;
import static epg.LanguagePropertyType.TOOLTIP_NEW_EPORTFOLIO;
import static epg.LanguagePropertyType.TOOLTIP_REMOVE_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_REMOVE_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_SAVE_AS_EPORTFOLIO;
import static epg.LanguagePropertyType.TOOLTIP_SAVE_EPORTFOLIO;
import static epg.LanguagePropertyType.TOOLTIP_SLIDESHOW;
import static epg.LanguagePropertyType.TOOLTIP_TEXT;
import static epg.LanguagePropertyType.TOOLTIP_VIDEO;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_BUTTONS;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_PANE;
import static epg.StartupConstants.CSS_CLASS_FILE_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_FILE_TOOLBAR_CONTAINER;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_CONTAINER;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_SITE_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_SITE_TOOLBAR;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_VIEW_PANE;
import static epg.StartupConstants.CSS_CLASS_PAGE_TITLE_LABEL;
import static epg.StartupConstants.CSS_CLASS_PAGE_TITLE_TEXT_FIELD;
import static epg.StartupConstants.CSS_CLASS_SELECTED_PAGE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SITE_TOOLBAR_CONTAINER;
import static epg.StartupConstants.CSS_CLASS_STUDENT_NAME_CONTAINER;
import static epg.StartupConstants.CSS_CLASS_STUDENT_NAME_LABEL;
import static epg.StartupConstants.CSS_CLASS_STUDENT_NAME_TEXT;
import static epg.StartupConstants.CSS_CLASS_TOP_WORKSPACE_PANE;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_CONTAINER;
import static epg.StartupConstants.ICON_ADD_PAGE;
import static epg.StartupConstants.ICON_BANNER_IMAGE;
import static epg.StartupConstants.ICON_EXIT;
import static epg.StartupConstants.ICON_EXPORT_EPORTFOLIO;
import static epg.StartupConstants.ICON_FOOTER_TEXT;
import static epg.StartupConstants.ICON_HYPERLINK_COMPONENT;
import static epg.StartupConstants.ICON_IMAGE_COMPONENT;
import static epg.StartupConstants.ICON_LOAD_EPORTFOLIO;
import static epg.StartupConstants.ICON_NEW_EPORTFOLIO;
import static epg.StartupConstants.ICON_REMOVE_PAGE;
import static epg.StartupConstants.ICON_SAVE_AS_EPORTFOLIO;
import static epg.StartupConstants.ICON_SAVE_EPORTFOLIO;
import static epg.StartupConstants.ICON_SLIDE_SHOW_COMPONENT;
import static epg.StartupConstants.ICON_TEXT_COMPONENT;
import static epg.StartupConstants.ICON_VIDEO_COMPONENT;
import static epg.StartupConstants.ICON_WINDOW_LOGO;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.ComponentSelectionController;
import epg.controller.FileController;
import epg.controller.PortfolioEditController;
import epg.controller.WorkspaceToolbarController;
import epg.error.ErrorHandler;
import epg.file.PortfolioFileManager;
import epg.model.Component;
import epg.model.Page;
import epg.model.PortfolioModel;
import epg.model.Text;
import static epg.view.TextComponentDialog.hyperlinkDialog;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class WorkspaceView {
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE 
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane epgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    VBox fileToolbarContainer;
    FlowPane fileToolbarPane;
    HBox studentNameContainer;
    Button newPortfolioButton;
    Button loadPortfolioButton;
    Button savePortfolioButton;
    Button saveAsPortfolioButton;
    Button exportPortfolioButton;
    Button exitButton;
    
    // FOR THE PORTFOLIO TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    //FOR THE PAGE TITLE
    FlowPane pageTitlePane;
    Label pageTitleLabel;
    TextField pageTitleTextField;
    
    //FOR THE TOP OF THE PAGE EDIT VIEW
    VBox topWorkspacePane;
    HBox pageEditContainer;
    HBox componentPane;
    FlowPane pageEditViewPane;
    Button bannerImageButton;
    TextField footerTextField;
    String highlightedText;
    Button hyperlinkBtn;
    Button footerBtn;
    Button removeHyperlinkBtn;
    GridPane bottomWorkspacePane;
    HBox hyperlinkBtnPane;
    ComboBox<String> layouts;
    ComboBox<String> colors;
    ComboBox<String> fonts;
    
    //FOR UNDERNEATH THE PAGE EDIT VIEW, THE COMPONENT ADDING CONTROLS
    FlowPane componentButtons;
    Button textComponent;
    Button imageComponent;
    Button videoComponent;
    Button slideShowComponent;
    
    //FOR THE WORKSPACE TOOLBAR
    TabPane workspacePane;
    HBox workspaceHBoxPane;
    Tab viewTab;
    Tab editTab;
    
    // THIS IS FOR THE SITE TOOLBAR
    HBox siteToolbar;
    Button addPageButton;
    Button removePageButton;
    ScrollPane pageEditorScrollPane;
    VBox pageSelectPane;
    VBox pagePane;
    
    //FOR THE COMPONENT EDIT TOOLBAR
    VBox componentEditPane;
    ScrollPane componentEditorScrollPane;
    Button removeBtn;
    Button editBtn;
    
    // WORKSPACE
    BorderPane workspace;

    // THIS IS THE PAGE WE'RE WORKING WITH
    PortfolioModel ePortfolio;

    // THIS IS FOR SAVING AND LOADING PAGES
    PortfolioFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO PORTFOLIO EDIT BUTTONS
    private PortfolioEditController editController;
    
    //CONTROLLER RESPONDS TO WORKSPACE TOOLBAR BUTTONS
    private WorkspaceToolbarController workspaceToolbarController;
    
    //CONTROLLER RESPONDS TO COMPONENT BUTTONS
    private ComponentSelectionController componentController;
    
    public WorkspaceView(PortfolioFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	ePortfolio = new PortfolioModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }
    
    // ACCESSOR METHODS
    public PortfolioModel getPortfolio() {
	return ePortfolio;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }
    
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();
        
        initComponentEditToolbar();
        
        initSiteToolbar();
        
        initWorkspaceModeToolbar();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }
    
    public void reloadPageEditWorkspace(){
        Page selectedPage = ePortfolio.getSelectedPage();
        
        if(selectedPage == null){
            componentPane.getChildren().clear();
            topWorkspacePane.getChildren().clear();
            bottomWorkspacePane.getChildren().clear();
        }
            
        else{
        //RELOAD COMBO BOXES FOR SELECTED PAGE
        if(!selectedPage.getFont().equals("default"))
            fonts.setValue(selectedPage.getFont());
        else
            fonts.setValue(fonts.getPromptText());
        
        if(!selectedPage.getColor().equals("default"))
            colors.setValue(selectedPage.getColor());
        else
            colors.setValue(colors.getPromptText());
        
        if(!selectedPage.getLayout().equals("default")){
            layouts.setValue(selectedPage.getLayout());
            bannerImageButton.setDisable(false);
        }
        else{
            layouts.setValue(layouts.getPromptText());
        }
        
        //RELOAD PAGE TITLE FOR SELECTED PAGE
        if(!selectedPage.getTitle().equals(DEFAULT_PAGE_TITLE))
            pageTitleTextField.setText(selectedPage.getTitle());
        else
            pageTitleTextField.setText("ENTER A PAGE TITLE");
        
        //CHECK IF BANNER IMAGE IS IN LAYOUT
        if(selectedPage.getLayout().equals("No Banner Image")){
            bannerImageButton.setDisable(true);
        }
        
        footerTextField.setText(selectedPage.getFooter());
        
        
        componentPane.getChildren().clear();
        topWorkspacePane.getChildren().clear();
        bottomWorkspacePane.getChildren().clear();
        componentPane.getChildren().add(componentButtons);
        topWorkspacePane.getChildren().addAll(pageEditContainer, componentPane);
        bottomWorkspacePane.add(footerTextField, 0, 0);
        bottomWorkspacePane.add(hyperlinkBtnPane, 0, 1);
        }
    }
    
    private void initWorkspace() {
	// FIRST THE WORKSPACE 
	workspace = new BorderPane();
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(CSS_CLASS_TOP_WORKSPACE_PANE);
        topWorkspacePane.setPrefWidth(Double.MAX_VALUE);
        pageEditContainer = new HBox();
        pageEditContainer.getStyleClass().add(CSS_CLASS_PAGE_EDIT_CONTAINER);
        pageEditContainer.setPrefWidth(Double.MAX_VALUE);
        componentPane = new HBox();
        componentPane.getStyleClass().add(CSS_CLASS_COMPONENT_PANE);
        componentPane.setPrefWidth(Double.MAX_VALUE);
        pageEditViewPane = new FlowPane();
        pageEditViewPane.setHgap(10);
        pageEditViewPane.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW_PANE);
        componentButtons = new FlowPane();
        componentButtons.getStyleClass().add(CSS_CLASS_COMPONENT_BUTTONS);
        componentButtons.setHgap(10);
        componentButtons.setPadding(new Insets(10, 10, 10, 10));
        
        pageEditContainer.setHgrow(pageEditViewPane, Priority.ALWAYS);
        pageEditContainer.getChildren().add(pageEditViewPane);
        componentPane.setHgrow(componentButtons, Priority.ALWAYS);
        
        
        
        //BANNER IMAGE SELECTION
        bannerImageButton = initChildButton(pageEditViewPane, ICON_BANNER_IMAGE,	TOOLTIP_BANNER_IMAGE,	    CSS_CLASS_PAGE_EDIT_TOOLBAR_BUTTON, false);
        
        //ENTERING FOOTER TEXT
        bottomWorkspacePane = new GridPane();
        hyperlinkBtnPane = new HBox(); 
        hyperlinkBtn = initChildButton(hyperlinkBtnPane, ICON_HYPERLINK_COMPONENT, TOOLTIP_HYPERLINK, CSS_CLASS_COMPONENT_BUTTON, true);
        footerBtn = initChildButton(hyperlinkBtnPane, ICON_FOOTER_TEXT, TOOLTIP_EDIT_HYPERLINK,  CSS_CLASS_COMPONENT_BUTTON, true);
        removeHyperlinkBtn = initChildButton(hyperlinkBtnPane, ICON_REMOVE_PAGE, TOOLTIP_REMOVE_COMPONENT, CSS_CLASS_COMPONENT_BUTTON, true );
        
	
       
	 
        footerTextField = new TextField();
        footerTextField.setEditable(true);
        footerTextField.getStyleClass().add(CSS_CLASS_STUDENT_NAME_TEXT);
	footerTextField.setPromptText("Enter Footer Text");
        
        
	
        //LAYOUT SELECTION
        layouts = new ComboBox<String>();
        layouts.getItems().addAll("Header On Top", "Nav Bar On Top", "Nav Bar On Left", "Prominent Banner Image", "No Banner Image");
        initComboBox(layouts, pageEditViewPane, TOOLTIP_LAYOUTS);
        layouts.setPromptText("Choose a Layout");
        
        //COLOR SELECTION
        colors = new ComboBox<String>();
        colors.getItems().addAll("Blue Is My Favorite Color", "Red Is My Favorite Color", "Green Is My Favorite Color", "Purple Is My Favorite Color", "I Don't Have A Favorite Color");
        initComboBox(colors, pageEditViewPane, TOOLTIP_COLORS);
        colors.setPromptText("Choose a Color Scheme");
        
        //PAGE FONT
        fonts = new ComboBox<String>();
        fonts.getItems().addAll("Montserrat", "Ubuntu", "Lobster", "IM Fell Great Primer", "Chewy");
        initComboBox(fonts, pageEditViewPane, TOOLTIP_FONTS);
        fonts.setPromptText("Choose a Font");
        
        //INITIALIZING COMPONENT BUTTONS
        textComponent = initChildButton(componentButtons, ICON_TEXT_COMPONENT, TOOLTIP_TEXT, CSS_CLASS_COMPONENT_BUTTON, false);
        imageComponent = initChildButton(componentButtons, ICON_IMAGE_COMPONENT, TOOLTIP_IMAGE, CSS_CLASS_COMPONENT_BUTTON, false);
        videoComponent = initChildButton(componentButtons, ICON_VIDEO_COMPONENT, TOOLTIP_VIDEO, CSS_CLASS_COMPONENT_BUTTON, false);
        slideShowComponent = initChildButton(componentButtons, ICON_SLIDE_SHOW_COMPONENT, TOOLTIP_SLIDESHOW, CSS_CLASS_COMPONENT_BUTTON, false);
        
	initTitleControls();
	
    }
    
    private void initEventHandlers() {
        //PORTFOLIO TITLE CONTROLS
        titleTextField.setOnAction(e -> {
            fileController.markAsEdited();
           reloadTitleControls();
        });
       
        //File Controls
        fileController = new FileController(this, fileManager);
        newPortfolioButton.setOnAction(e -> {
	    fileController.handleNewPortfolioRequest();
	});
	loadPortfolioButton.setOnAction(e -> {
	    fileController.handleLoadPortfolioRequest();
	});
	savePortfolioButton.setOnAction(e -> {
	    fileController.handleSavePortfolioRequest();
	});
        saveAsPortfolioButton.setOnAction(e -> {
	    fileController.handleSaveAsPortfolioRequest();
	});
	exportPortfolioButton.setOnAction(e -> {
            fileController.handleExportPortfolioRequest();
           
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
        
        //TAB PANE CONTROLS FOR PAGE EDIT WORKSPACE. THIS IS WHERE
        //WE RELOAD PAGE EDIT WORKSPACE TO FIRST PAGE OF EPORTFOLIO SHOWING IN WORKSPACE.
        //THIS MEANS THE FIRST PAGE WILL AUTOMATICALLY BE SELECTED UPON RELOADING
        
        //TAB PANE CONTROLS FOR PREVIEW OF SITE
        viewTab.setOnSelectionChanged(e ->{
            File webFile = new File("base/index.html");
        
        String path = webFile.getAbsolutePath().replace("./","");
        WebView site = new WebView();
        site.getEngine().load("file:///" + path);
        
        epgPane.getChildren().clear();
        epgPane.setTop(fileToolbarContainer);
        epgPane.setCenter(site);
        });
        
        //PAGE EDIT CONTROLS 
        //PAGE TITLE TEXT FIELD CONTROL
        pageTitleTextField.setOnAction(e ->{
           pageTitleTextField.setText(ePortfolio.getSelectedPage().getTitle()); 
           fileController.markAsEdited();
           reloadTitleControls();
           reloadSitePane();
        });
        
	editController = new PortfolioEditController(this);
	addPageButton.setOnAction(e -> {
	    editController.processAddPageRequest();
            fileController.markAsEdited();
	});
	removePageButton.setOnAction(e -> {
	    editController.processRemovePageRequest();
            fileController.markAsEdited();
	});
        
        //WORKSPACE TOOLBAR CONTROLS
        workspaceToolbarController = new WorkspaceToolbarController(this);
        //BANNER IMAGE
        bannerImageButton.setOnAction(e ->{
            workspaceToolbarController.processBannerImageRequest(ePortfolio, this);
            fileController.markAsEdited();
            reloadTitleControls();
          });
        //FOOTER TEXT
        footerTextField.setOnMouseClicked(e ->{
            if(highlightedText != null && highlightedText.equals(footerTextField.getSelectedText())){
                footerBtn.setDisable(false);
            }
             highlightedText = footerTextField.getSelectedText();
             if(footerBtn.isDisabled() && highlightedText.length() > 0){
                hyperlinkBtn.setDisable(false);
            }
            else{
                hyperlinkBtn.setDisable(true);
            }
        });
        footerTextField.setOnAction(e->{
            ePortfolio.getSelectedPage().setFooter(footerTextField.getText());
            fileController.markAsEdited();
            reloadPageEditWorkspace();
            
        });
       
        //FOOTERBTN AND REMOVEHYPERLINKBTN ACTIONS. THINK I NEED TO WORK ON OTHER COMPONENTS FIRST, THEN 
        //WHEN I CREATE THE HYPERLINK OBJECT CLASS, WORK ON THIS AGAIN. 
        
        
        
         hyperlinkBtn.setOnMouseClicked(e->{
             if(highlightedText.length() > 0){
                 hyperlinkDialog(new Text(highlightedText, fonts.getValue(), true));
             }
         });
         
        //FONT COMBO BOX 
        fonts.setOnAction(e ->{
            ePortfolio.getSelectedPage().setFont(fonts.getValue());
            fileController.markAsEdited();
           reloadTitleControls();
            reloadPageEditWorkspace();
        });
        //COLOR COMBO BOX
        colors.setOnAction( e->{
           ePortfolio.getSelectedPage().setColor(colors.getValue());
           fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });
        //LAYOUT COMBO BOX
        layouts.setOnAction( e->{
           ePortfolio.getSelectedPage().setLayout(layouts.getValue());
           fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });   
        
        //COMPONENT CONTROLS
        componentController = new ComponentSelectionController(this);
        textComponent.setOnAction(e -> {
            componentController.processAddText(ePortfolio.getSelectedPage(), this);
            fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });
        imageComponent.setOnAction(e ->{
           componentController.processSelectImage(ePortfolio.getSelectedPage(), this);
           fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });
        videoComponent.setOnAction(e ->{
           componentController.processSelectVideo(ePortfolio.getSelectedPage(), this); 
           fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });
        slideShowComponent.setOnAction(e ->{
            componentController.processSelectSlideshow(ePortfolio.getSelectedPage(), this);
            fileController.markAsEdited();
           reloadTitleControls();
           reloadPageEditWorkspace();
        });
        
    }
    
    public void reloadComponentEditToolbar(){
        componentEditPane.getChildren().clear();
        Page selected = ePortfolio.getSelectedPage();
	for (Component comp : selected.getComponents()) {
	    ComponentEditView compEditor = new ComponentEditView(comp, selected, this);
            removeBtn = compEditor.getRemoveBtn();
            editBtn = compEditor.getEditBtn();
	    if (selected.isSelectedComponent(comp)){
		compEditor.getStyleClass().add(CSS_CLASS_SELECTED_PAGE_EDIT_VIEW);
                removeBtn.setDisable(false);
                editBtn.setDisable(false);
            }
            else{
		compEditor.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
                removeBtn.setDisable(true);
                editBtn.setDisable(true);
            }
	    componentEditPane.getChildren().add(compEditor);
	    compEditor.setOnMousePressed(e -> {
		selected.setSelectedComponent(comp);
		this.reloadComponentEditToolbar();
	    });
	}
        
	updateSiteEditToolbarControls();
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.setHgap(10);
        fileToolbarPane.setPadding(new Insets(10, 10, 10, 10));
        studentNameContainer = new HBox();
        studentNameContainer.getStyleClass().add(CSS_CLASS_STUDENT_NAME_CONTAINER);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO,	TOOLTIP_NEW_EPORTFOLIO,	    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
	loadPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO,	TOOLTIP_LOAD_EPORTFOLIO,    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
	savePortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO,	TOOLTIP_SAVE_EPORTFOLIO,    CSS_CLASS_FILE_TOOLBAR_BUTTON, true);
        saveAsPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_EPORTFOLIO,	TOOLTIP_SAVE_AS_EPORTFOLIO,    CSS_CLASS_FILE_TOOLBAR_BUTTON, true);
	exportPortfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT_EPORTFOLIO,	TOOLTIP_EXPORT_EPORTFOLIO,    CSS_CLASS_FILE_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
    }
    
    private void initComboBox(ComboBox<String> comboBox, Pane toolbar, LanguagePropertyType tooltip){
        //CONSIDERING CHANGING THIS METHOD SO THAT IT TAKES PANE AND TOOLTIP AS PARAMETER
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	ComboBox<String> menu = comboBox;
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	menu.setTooltip(buttonTooltip);
	toolbar.getChildren().add(menu);
    }
    
    private void initWorkspaceModeToolbar(){
        workspaceHBoxPane = new HBox();
        workspaceHBoxPane.getStyleClass().add(CSS_CLASS_WORKSPACE_CONTAINER);
        workspacePane = new TabPane();
        
        editTab = new Tab();
        editTab.setText("Page Edit Workspace");
        viewTab = new Tab();
        viewTab.setText("Site View Workspace");
        
        workspacePane.getTabs().addAll(editTab, viewTab);
        
        
        
    }
    
    private void initComponentEditToolbar(){
        //WHERE COMPONENT EDIT VIEW WILL GO
        componentEditPane = new VBox();
        componentEditPane.setMinHeight(500);
        
        //SCROLL PANE TO HOLD COMPONENT EDIT VIEW HOLDER
        componentEditorScrollPane = new ScrollPane();
        componentEditorScrollPane.setPrefWidth(200);
        componentEditorScrollPane.setContent(componentEditPane);
        
    }
    
    private void initSiteToolbar() {
        // THIS IS THE SITE TOOLBAR AND GOES DOWN THE LEFT SIDE
	siteToolbar = new HBox();
        siteToolbar.setPrefWidth(200);
	siteToolbar.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR);
        //THE ADD AND REMOVE PAGE CONTROLS ARE AT THE TOP OF THE TOOLBAR SIDE BY SIDE
        FlowPane pageControlPane = new FlowPane();
	addPageButton = this.initChildButton(pageControlPane,		ICON_ADD_PAGE,	    TOOLTIP_ADD_PAGE,	    CSS_CLASS_SITE_TOOLBAR_BUTTON,  true);
	removePageButton = this.initChildButton(pageControlPane,	ICON_REMOVE_PAGE,  TOOLTIP_REMOVE_PAGE,   CSS_CLASS_SITE_TOOLBAR_BUTTON,  true);
	//THE PAGE CONTROLS ARE ADDED TO THE TOOLBAR
        siteToolbar.getChildren().add(pageControlPane);
        
	// THIS IS THE PANE THAT ALLOWS USERS TO SELECT PAGES
	pageSelectPane = new VBox();
        pageSelectPane.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR_CONTAINER);
        pageSelectPane.setPrefWidth(200);
        pageSelectPane.setMinHeight(500);
        //IF THERE ARE A LOT OF PAGES USER CAN SCROLL THROUGH 
	pageEditorScrollPane = new ScrollPane();
        //PLACE VBOX IN SCROLLPANE TO HOLD PAGES
        pagePane = new VBox();
        pagePane.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
        pagePane.setPrefWidth(200);
        pagePane.setPrefHeight(500);
        pagePane.setPickOnBounds(false);
        pageEditorScrollPane.setPickOnBounds(false);
        pageSelectPane.setPickOnBounds(false);
        pageEditorScrollPane.setContent(pagePane);
        //ADD PAGE SCROLL PANE TO SITE TOOLBAR UNDERNEATH PAGE CONTROLS
        pageSelectPane.getChildren().addAll(siteToolbar, pageEditorScrollPane);
    }
    
     private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);
        // Set the Icon on the Stage
        primaryStage.getIcons().add(new Image("file:" + PATH_ICONS+ ICON_WINDOW_LOGO));

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        fileToolbarContainer = new VBox();
        fileToolbarContainer.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_CONTAINER);
        fileToolbarContainer.getChildren().add(fileToolbarPane);
        fileToolbarContainer.getChildren().add(workspaceHBoxPane);
        fileToolbarContainer.getChildren().add(studentNameContainer);
	epgPane = new BorderPane();
	epgPane.setTop(fileToolbarContainer);
        primaryScene = new Scene(epgPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    public Button initChildButton(
            Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    public void updateToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        epgPane.setLeft(pageSelectPane);
	epgPane.setCenter(workspace);
        workspace.setTop(topWorkspacePane);
        workspace.setLeft(componentEditorScrollPane);
        workspace.setBottom(bottomWorkspacePane);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	savePortfolioButton.setDisable(saved);
        saveAsPortfolioButton.setDisable(saved);
	exportPortfolioButton.setDisable(false);
	
	updateSiteEditToolbarControls();
    }
    
    
    
    public void initWorkspaceModeToolbarControls(){
        //NEED TO FIGURE OUT WHERE TO PUT THIS!!!
        if(workspaceHBoxPane.getChildren().size() > 0)
            workspaceHBoxPane.getChildren().clear();
        workspaceHBoxPane.getChildren().add(workspacePane);
        workspacePane.setStyle("-fx-padding: 2 0 0 0");
    }
    
    public void updateSiteEditToolbarControls() {
        // AND THE SLIDESHOW EDIT TOOLBAR
	addPageButton.setDisable(false);
	boolean pageSelected = ePortfolio.isPageSelected();
	removePageButton.setDisable(!pageSelected);
    }
    
    public void reloadSitePane() {
        if(ePortfolio.getPages().size() == 0){
            workspace.getChildren().clear();
        }
            
        pagePane.getChildren().clear();
	reloadTitleControls();
        
	for (Page page : ePortfolio.getPages()) {
	    PageEditView pageEditor = new PageEditView(page);
	    if (ePortfolio.isSelectedPage(page))
		pageEditor.getStyleClass().add(CSS_CLASS_SELECTED_PAGE_EDIT_VIEW);
	    else
		pageEditor.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
	    pagePane.getChildren().add(pageEditor);
            reloadComponentEditToolbar();
	    pageEditor.setOnMousePressed(e -> {
		ePortfolio.setSelectedPage(page);
		this.reloadSitePane();
	    });
            
	}
        reloadPageEditWorkspace();
        if(ePortfolio.getPages().size() > 0){
            removePageButton.setDisable(false);
        }
        else
            removePageButton.setDisable(true);
            
	updateSiteEditToolbarControls();
    }
    
    
    private void initTitleControls() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String labelPrompt = props.getProperty(LABEL_EPORTFOLIO_TITLE);
        titlePane = new FlowPane();
	titleLabel = new Label(labelPrompt);
        titleLabel.getStyleClass().add(CSS_CLASS_STUDENT_NAME_LABEL);
	titleTextField = new TextField();
        titleTextField.getStyleClass().add(CSS_CLASS_STUDENT_NAME_TEXT);
	
        titlePane.setStyle("-fx-padding: 5 0 10 5");
        titleLabel.setStyle("-fx-font-weight: bold");
	titlePane.getChildren().add(titleLabel);
	titlePane.getChildren().add(titleTextField);
       
        
	String titlePrompt = props.getProperty(LanguagePropertyType.DEFAULT_EPORTFOLIO_TITLE);
	titleTextField.setText(titlePrompt);
        
	
	titleTextField.textProperty().addListener(e -> {
	    ePortfolio.setTitle(titleTextField.getText());
	});
        
        String pageLabelPrompt = props.getProperty(LABEL_PAGE_TITLE);
	pageTitlePane = new FlowPane();
        pageTitlePane.setStyle("-fx-padding: 5 0 5 15");
	pageTitleLabel = new Label(pageLabelPrompt);
        pageTitleLabel.getStyleClass().add(CSS_CLASS_PAGE_TITLE_LABEL);
	pageTitleTextField = new TextField();
        pageTitleTextField.getStyleClass().add(CSS_CLASS_PAGE_TITLE_TEXT_FIELD);
	
	pageTitlePane.getChildren().addAll(pageTitleLabel, pageTitleTextField);
        
	
	String pageTitlePrompt = props.getProperty(LanguagePropertyType.DEFAULT_PAGE_TITLE);
	pageTitleTextField.setText(pageTitlePrompt);
        
	
	pageTitleTextField.textProperty().addListener(e -> {
            ePortfolio.getSelectedPage().setTitle(pageTitleTextField.getText());
	});
        
        pageEditViewPane.getChildren().add(pageTitlePane);
    }
     
     public void reloadTitleControls() {
         studentNameContainer.getChildren().clear();
         studentNameContainer.getChildren().add(titlePane);
         titlePane.getChildren().clear();
         titlePane.getChildren().addAll(titleLabel, titleTextField);
         titleTextField.setText(ePortfolio.getTitle());
     }
            
            
            
}
