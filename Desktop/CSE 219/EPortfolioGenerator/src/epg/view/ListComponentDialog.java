/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_LIST_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LIST_LABEL;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class ListComponentDialog extends Stage{
    private ArrayList<String> list; 
    
        public ListComponentDialog(){
        this.setTitle("Add List");
        
        ArrayList<String> tempList = new ArrayList<>();
        list = new ArrayList<>();
        ObservableList<String> listItems = FXCollections.observableArrayList();
        
        Button btnAdd = new Button("Add Item");
        Button btnDelete = new Button("Remove Item");
        Button btnSubmit = new Button("Submit");
        HBox buttonHBox = new HBox();
        buttonHBox.setStyle("-fx-padding: 10 0 0 0");
        Label addTextLabel = new Label("Enter Text Here");
        addTextLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        addTextLabel.setStyle("-fx-padding: 2 0 2 5");
        Label titleLabel = new Label("Your List");
        titleLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        titleLabel.setStyle("-fx-padding: 2 0 2 5");
       
        ListView<String> listBox = new ListView<String>();
        VBox listHolder = new VBox();
        listHolder.setStyle("-fx-background-color: #cdcdc0");
        
        TextField textField = new TextField();
        textField.setPromptText("Enter List Item");
        textField.setStyle("-fx-padding: 5 0 10 0");
        
        buttonHBox.getChildren().addAll(btnAdd, btnDelete, btnSubmit);
        listBox.setItems(listItems);
        listHolder.getChildren().addAll(titleLabel, listBox, addTextLabel, textField, buttonHBox);
        
        btnAdd.setDisable(true);
        btnDelete.setDisable(true);
        
        textField.setOnMouseClicked(e ->{
            btnAdd.setDisable(false);
        });
        
        listBox.setOnMouseClicked(e ->{
            if(listBox.getSelectionModel().getSelectedItem().length() > listItems.size());
                btnDelete.setDisable(false);
        });
        
        btnAdd.setOnAction(e ->{
            listItems.add(textField.getText());
            tempList.add(textField.getText());
            textField.deleteText(0, textField.getText().length());
            textField.setPromptText("Enter List Item");
            btnDelete.setDisable(true);
        });
        
        btnDelete.setOnAction(ev ->{
            int selectedItem = listBox.getSelectionModel().getSelectedIndex();
            listItems.remove(selectedItem);
            tempList.remove(selectedItem);
            if(listItems.isEmpty())
                btnDelete.setDisable(true);
        });
        
        btnSubmit.setOnAction(eve ->{
            list = tempList;
            this.hide();
        });
        
        btnAdd.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        btnDelete.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        btnSubmit.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        
        Scene scene = new Scene(listHolder);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(scene);
        
        
    }
        
         public ListComponentDialog(Component comp){
        this.setTitle("Edit List");
        
        ArrayList<String> tempList = comp.getList();
        list = comp.getList();
        ObservableList<String> listItems = FXCollections.observableArrayList();
        
        for(String s: tempList){
            listItems.add(s);
        }
        
        Button btnAdd = new Button("Add Item");
        Button btnDelete = new Button("Remove Item");
        Button btnSubmit = new Button("Submit");
        HBox buttonHBox = new HBox();
        buttonHBox.setStyle("-fx-padding: 10 0 0 0");
        Label addTextLabel = new Label("Enter Text Here");
        addTextLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        addTextLabel.setStyle("-fx-padding: 2 0 2 5");
        Label titleLabel = new Label("Your List");
        titleLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        titleLabel.setStyle("-fx-padding: 2 0 2 5");
       
        ListView<String> listBox = new ListView<String>();
        VBox listHolder = new VBox();
        listHolder.setStyle("-fx-background-color: #cdcdc0");
        
        TextField textField = new TextField();
        textField.setPromptText("Enter List Item");
        textField.setStyle("-fx-padding: 5 0 10 0");
        
        buttonHBox.getChildren().addAll(btnAdd, btnDelete, btnSubmit);
        listBox.setItems(listItems);
        listHolder.getChildren().addAll(titleLabel, listBox, addTextLabel, textField, buttonHBox);
        
        btnAdd.setDisable(true);
        btnDelete.setDisable(true);
        
        textField.setOnMouseClicked(e ->{
            btnAdd.setDisable(false);
        });
        
        listBox.setOnMouseClicked(e ->{
            if(listBox.getSelectionModel().getSelectedItem().length() > listItems.size());
                btnDelete.setDisable(false);
        });
        
        btnAdd.setOnAction(e ->{
            listItems.add(textField.getText());
            tempList.add(textField.getText());
            textField.deleteText(0, textField.getText().length());
            textField.setPromptText("Enter List Item");
            btnDelete.setDisable(true);
        });
        
        btnDelete.setOnAction(ev ->{
            int selectedItem = listBox.getSelectionModel().getSelectedIndex();
            listItems.remove(selectedItem);
            tempList.remove(selectedItem);
            if(listItems.isEmpty())
                btnDelete.setDisable(true);
        });
        
        btnSubmit.setOnAction(eve ->{
            list = tempList;
            this.hide();
        });
        
        btnAdd.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        btnDelete.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        btnSubmit.getStyleClass().add(CSS_CLASS_LIST_BUTTON);
        
        Scene scene = new Scene(listHolder);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(scene);
        
        
    }
    
    public ArrayList<String> getList(){return list;}
}
