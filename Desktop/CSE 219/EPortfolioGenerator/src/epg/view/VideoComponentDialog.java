/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_COMP_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LIST_LABEL;
import static epg.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static epg.StartupConstants.LABEL_IMAGE_COMPONENT_DIALOG;
import static epg.StartupConstants.LABEL_VID_COMPONENT_DIALOG;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.PATH_VIDS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.file.PortfolioFileManager.SLASH;
import epg.model.Component;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class VideoComponentDialog extends Stage{
    GridPane gridPane;
    Label dialogLabel;
    Label vidFileLabel;
    Label widthLabel;
    Label heightLabel;
    Button submitButton;
    Button vidBrowseBtn;
    String fileName;
    String path;
    String tempPath;
    String caption;
    String tempFileName;
    int vidHeight;
    int vidWidth;
    TextField vidWidthTextField;
    TextField vidHeightTextField;
    TextField vidFileTextField;
    TextField captionTextField;
    Label captionLabel;
    FileChooser vidFileChooser;
    
    public VideoComponentDialog(){
        
        
        this.setTitle(LABEL_VID_COMPONENT_DIALOG);
        
        dialogLabel = new Label(LABEL_VID_COMPONENT_DIALOG + ":");
        
        vidFileLabel = new Label("Select a Video File:");
        vidFileLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        widthLabel = new Label("Enter Video Width:");
        widthLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        heightLabel = new Label("Enter Video Height:");
        heightLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        captionLabel = new Label("Enter Caption:");
        captionLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        
        vidWidthTextField = new TextField();
        vidHeightTextField = new TextField();
        vidFileTextField = new TextField();
        captionTextField = new TextField();
        
        vidBrowseBtn = new Button("BROWSE");
        vidBrowseBtn.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        
        vidFileChooser = new FileChooser();
        // SET THE STARTING DIRECTORY
	vidFileChooser.setInitialDirectory(new File(PATH_VIDS));
	
        vidBrowseBtn.setOnMouseClicked(e ->{
            // LET'S OPEN THE FILE CHOOSER
            File file = vidFileChooser.showOpenDialog(null);
            if (file != null) {
                tempPath = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                tempFileName = file.getName();
            
                String vidPath = tempPath + SLASH + tempFileName;
                File vidFile = new File(vidPath);
                vidFileTextField.setText(vidPath);
                
                // GET AND SET THE IMAGE
                URL fileURL;
                try {
                    fileURL = vidFile.toURI().toURL();
                
                
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
       
        
	submitButton = new Button("SUBMIT");
        submitButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
	
        //EVENT HANDLER FOR VIDEO DIMENSIONS
        submitButton.setOnAction(e ->{
            int videoWidth = 0;
            int videoHeight = 0;
            try{
                String text = vidWidthTextField.getText();
                videoWidth = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
                vidWidthTextField.setText("Enter a value from 1 to 1000");
            }
            try{
                String text = vidHeightTextField.getText();
                videoHeight = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
                vidHeightTextField.setText("Enter a value from 1 to 1000");
            }
            
            if(videoWidth > 0 && videoWidth <= 1000 && videoHeight > 0 && videoHeight <= 1000
                    && tempFileName != null && captionTextField.getText().length() > 0 
                    && tempFileName.substring(tempFileName.length() - 4).equals(".mp4")){
                vidWidth = videoWidth;
                vidHeight = videoHeight;
                fileName = tempFileName;
                path = tempPath;
                caption = captionTextField.getText();
                this.hide();
            }
            else{
                vidWidthTextField.setText("Enter a value from 1 to 1000");
                vidHeightTextField.setText("Enter a value from 1 to 1000");
                captionTextField.setText("Enter Caption");
                vidFileTextField.setText("File Extensions: .mp4");
            }
        });
        
	gridPane = new GridPane();
	gridPane.add(vidFileLabel, 0, 0);
	gridPane.add(vidFileTextField, 1, 0);
	gridPane.add(vidBrowseBtn, 2, 0);
        gridPane.add(widthLabel, 0, 1);
        gridPane.add(vidWidthTextField, 1,1);
        gridPane.add(heightLabel, 0, 2);
        gridPane.add(vidHeightTextField, 1, 2);
        gridPane.add(captionLabel, 0, 3);
        gridPane.add(captionTextField, 1, 3);
        gridPane.add(submitButton, 0, 4);
	
	
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_IMAGE_DIALOG_PANE);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        
    }
    
    public VideoComponentDialog(Component comp){
        
        
        this.setTitle("EDIT VIDEO");
        
        dialogLabel = new Label(LABEL_VID_COMPONENT_DIALOG + ":");
        
        vidFileLabel = new Label("Select a Video File:");
        vidFileLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        widthLabel = new Label("Enter Video Width:");
        widthLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        heightLabel = new Label("Enter Video Height:");
        heightLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        captionLabel = new Label("Enter Caption:");
        captionLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        
        vidWidthTextField = new TextField();
        vidWidthTextField.setText(String.valueOf(comp.getVideoWidth()));
        vidHeightTextField = new TextField();
        vidHeightTextField.setText(String.valueOf(comp.getVideoHeight()));
        vidFileTextField = new TextField();
        captionTextField = new TextField();
        captionTextField.setText(comp.getVideoCaption());
        
        tempPath = comp.getVideoPath();
        tempFileName = comp.getVideoFileName();
        String videoPath = comp.getVideoPath() + comp.getVideoFileName();
                File videoFile = new File(videoPath);
                vidFileTextField.setText(videoPath);
                
                // GET AND SET THE IMAGE
                URL initFileURL;
                try {
                    initFileURL = videoFile.toURI().toURL();
                
                
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
        
        
        vidBrowseBtn = new Button("BROWSE");
        vidBrowseBtn.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        
        vidFileChooser = new FileChooser();
        // SET THE STARTING DIRECTORY
	vidFileChooser.setInitialDirectory(new File(PATH_VIDS));
	
        vidBrowseBtn.setOnMouseClicked(e ->{
            // LET'S OPEN THE FILE CHOOSER
            File file = vidFileChooser.showOpenDialog(null);
            if (file != null) {
                tempPath = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                tempFileName = file.getName();
            
                String vidPath = tempPath + SLASH + tempFileName;
                File vidFile = new File(vidPath);
                vidFileTextField.setText(vidPath);
                
                // GET AND SET THE IMAGE
                URL fileURL;
                try {
                    fileURL = vidFile.toURI().toURL();
                
                
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
       
        
	submitButton = new Button("SUBMIT");
        submitButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
	
        //EVENT HANDLER FOR VIDEO DIMENSIONS
        submitButton.setOnAction(e ->{
            int videoWidth = 0;
            int videoHeight = 0;
            try{
                String text = vidWidthTextField.getText();
                videoWidth = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
                vidWidthTextField.setText("Enter a value from 1 to 1000");
            }
            try{
                String text = vidHeightTextField.getText();
                videoHeight = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
                vidHeightTextField.setText("Enter a value from 1 to 1000");
            }
            
            if(videoWidth > 0 && videoWidth <= 1000 && videoHeight > 0 && videoHeight <= 1000
                    && tempFileName != null && captionTextField.getText().length() > 0 
                    && tempFileName.substring(tempFileName.length() - 4).equals(".mp4")){
                vidWidth = videoWidth;
                vidHeight = videoHeight;
                fileName = tempFileName;
                path = tempPath;
                caption = captionTextField.getText();
                this.hide();
            }
            else{
                vidWidthTextField.setText("Enter a value from 1 to 1000");
                vidHeightTextField.setText("Enter a value from 1 to 1000");
                captionTextField.setText("Enter Caption");
                vidFileTextField.setText("File Extensions: .mp4");
            }
        });
        
	gridPane = new GridPane();
	gridPane.add(vidFileLabel, 0, 0);
	gridPane.add(vidFileTextField, 1, 0);
	gridPane.add(vidBrowseBtn, 2, 0);
        gridPane.add(widthLabel, 0, 1);
        gridPane.add(vidWidthTextField, 1,1);
        gridPane.add(heightLabel, 0, 2);
        gridPane.add(vidHeightTextField, 1, 2);
        gridPane.add(captionLabel, 0, 3);
        gridPane.add(captionTextField, 1, 3);
        gridPane.add(submitButton, 0, 4);
	
	
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_IMAGE_DIALOG_PANE);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        
    }
    
    public String getVideoPath(){return path;}
     public String getVideoFileName(){return fileName;}
    public int getVideoWidth(){return vidWidth;}
    public int getVideoHeight() {return vidHeight;}
    public String getVideoCaption(){return caption;}
}
