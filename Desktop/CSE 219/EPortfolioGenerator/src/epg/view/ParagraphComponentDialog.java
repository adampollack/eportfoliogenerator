/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.TOOLTIP_EDIT_HYPERLINK;
import static epg.LanguagePropertyType.TOOLTIP_HYPERLINK;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.ICON_FOOTER_TEXT;
import static epg.StartupConstants.ICON_HYPERLINK_COMPONENT;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.ParagraphComponent;
import epg.model.Text;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class ParagraphComponentDialog extends Stage{
    GridPane grid;
    Label paragraphLabel;
    Button hyperlinkBtn;
    Image buttonImage;
    TextArea paragraphArea;
    Button editBtn;
    Button submit;
    VBox btnPane;
    String paragraph;
    String highlightedText;
    ComboBox<String> fonts;
    
    public ParagraphComponentDialog() {
        this.setTitle("Add Paragraph");
        
        
        btnPane = new VBox();
        hyperlinkBtn = new Button();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + ICON_HYPERLINK_COMPONENT;
	buttonImage = new Image(imagePath);
	hyperlinkBtn.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_HYPERLINK.toString()));
	hyperlinkBtn.setTooltip(buttonTooltip);
        hyperlinkBtn.setStyle("-fx-background-color: black");
        hyperlinkBtn.setDisable(true);
        
        editBtn = new Button();
	imagePath = "file:" + PATH_ICONS + ICON_FOOTER_TEXT;
	buttonImage = new Image(imagePath);
	editBtn.setGraphic(new ImageView(buttonImage));
	buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_EDIT_HYPERLINK.toString()));
	editBtn.setTooltip(buttonTooltip);
        editBtn.setStyle("-fx-background-color: black");
        editBtn.setDisable(true);
                
        
        paragraphArea = new TextArea();
        paragraphArea.setWrapText(true);
        paragraphArea.setEditable(true);
        
        paragraphLabel = new Label("Enter Your Text:");
        
        ObservableList<String> fontChoices = FXCollections.observableArrayList();
	fontChoices.add("Muli");
	fontChoices.add("Oswald");
        fontChoices.add("Slabo");
        fontChoices.add("Lato");
        fontChoices.add("Open Sans");
        fonts = new ComboBox<String>(fontChoices);
        fonts.setPromptText("Choose a Font");
        
        submit = new Button("SUBMIT");
        
        
        
          
        
        submit.setOnAction(e -> {
            if(paragraphArea.getText().length() > 0){
                paragraph = paragraphArea.getText();
                this.hide();
            }
            else{
                paragraphArea.setPromptText("ENTER TEXT HERE");
            }
        });
	
        btnPane.getChildren().addAll(hyperlinkBtn, editBtn);
	grid = new GridPane();
	grid.add(paragraphLabel, 0, 0);
        grid.add(btnPane, 1, 1);
        grid.add(paragraphArea, 0, 1);
	grid.add(fonts, 2, 0);
        grid.add(submit, 0, 2);
        
       
        
        hyperlinkBtn.getStyleClass().add(CSS_CLASS_COMPONENT_BUTTON);
        paragraphLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene scene = new Scene(grid);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(scene);
        
    }
    
    ParagraphComponentDialog(Component comp){
        this.setTitle("Edit Paragraph");
        
        
        btnPane = new VBox();
        hyperlinkBtn = new Button();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + ICON_HYPERLINK_COMPONENT;
	buttonImage = new Image(imagePath);
	hyperlinkBtn.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_HYPERLINK.toString()));
	hyperlinkBtn.setTooltip(buttonTooltip);
        hyperlinkBtn.setStyle("-fx-background-color: black");
        hyperlinkBtn.setDisable(true);
        
        editBtn = new Button();
	imagePath = "file:" + PATH_ICONS + ICON_FOOTER_TEXT;
	buttonImage = new Image(imagePath);
	editBtn.setGraphic(new ImageView(buttonImage));
	buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_EDIT_HYPERLINK.toString()));
	editBtn.setTooltip(buttonTooltip);
        editBtn.setStyle("-fx-background-color: black");
        editBtn.setDisable(true);
                
        
        paragraphArea = new TextArea();
        paragraphArea.setWrapText(true);
        paragraphArea.setEditable(true);
        
        paragraphLabel = new Label("Enter Your Text:");
        paragraphArea.setText(comp.getParagraph());
        
        ObservableList<String> fontChoices = FXCollections.observableArrayList();
	fontChoices.add("Muli");
	fontChoices.add("Oswald");
        fontChoices.add("Slabo");
        fontChoices.add("Lato");
        fontChoices.add("Open Sans");
        fonts = new ComboBox<String>(fontChoices);
        fonts.setValue(comp.getFont());
        
        submit = new Button("SUBMIT");
        
        
        
          
        
        submit.setOnAction(e -> {
            if(paragraphArea.getText().length() > 0){
                paragraph = paragraphArea.getText();
                this.hide();
            }
            else{
                paragraphArea.setPromptText("ENTER TEXT HERE");
            }
        });
	
        btnPane.getChildren().addAll(hyperlinkBtn, editBtn);
	grid = new GridPane();
	grid.add(paragraphLabel, 0, 0);
        grid.add(btnPane, 1, 1);
        grid.add(paragraphArea, 0, 1);
	grid.add(fonts, 2, 0);
        grid.add(submit, 0, 2);
        
       
        
        hyperlinkBtn.getStyleClass().add(CSS_CLASS_COMPONENT_BUTTON);
        paragraphLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene scene = new Scene(grid);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(scene);
    }
    
     public static Text hyperlinkDialog(Text word){
        Stage stage = new Stage();
        stage.setTitle("Add Hyperlink URL");
        
        TextField header = new TextField();
        header.setStyle("-fx-font-size: 14pt");
        header.setEditable(true);
        
        Label headerLabel = new Label("Enter the URL:");
        
        Button submit = new Button("SUBMIT");
	
	GridPane grid = new GridPane();
        grid.setVgap(5);
	grid.add(headerLabel, 0, 0, 2, 1);
	grid.add(header, 0, 1, 1, 1);
        grid.add(submit, 0, 2, 1, 1);
        
        submit.setOnAction(e -> {
            if(header.getText() != null){
                word.makeHyperlink(header.getText());
                stage.hide();
            }
            else
                header.setText("Enter a URL");
        });
        
        headerLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene headerDialog = new Scene(grid);
        
        headerDialog.getStylesheets().add(STYLE_SHEET_UI);
        stage.setScene(headerDialog);
        stage.show();
        
        return word;
    }
     public String getParagraph(){return paragraph;}
    
     public String getFont(){
         if(fonts.getValue() == null)
             return "Muli";
         return fonts.getValue();
     }
    
}
