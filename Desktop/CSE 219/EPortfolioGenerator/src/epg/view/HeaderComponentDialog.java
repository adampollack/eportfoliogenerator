/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class HeaderComponentDialog extends Stage{
    private String header;
    
    public HeaderComponentDialog(){
        this.setTitle("Add Header");
        
        TextField head = new TextField();
        head.setStyle("-fx-font-size: 14pt");
        head.setEditable(true);
        
        Label headerLabel = new Label("Enter Your Text:");
        
        Button submit = new Button("SUBMIT");
	
	GridPane grid = new GridPane();
        grid.setVgap(5);
	grid.add(headerLabel, 0, 0, 2, 1);
	grid.add(head, 0, 1, 1, 1);
        grid.add(submit, 0, 2, 1, 1);
        
        submit.setOnAction(e -> {
            if(head.getText().length() > 0){
                header = head.getText();
                this.hide();
            }
            else{
                head.setPromptText("Enter Header Text Here");
            }
        });
        
        headerLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene scene = new Scene(grid);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        setScene(scene);
    
    }
    
    public HeaderComponentDialog(Component comp){
        this.setTitle("Add Header");
        
        TextField head = new TextField();
        head.setText(comp.getHeader());
        head.setStyle("-fx-font-size: 14pt");
        head.setEditable(true);
        
        Label headerLabel = new Label("Enter Your Text:");
        
        Button submit = new Button("SUBMIT");
	
	GridPane grid = new GridPane();
        grid.setVgap(5);
	grid.add(headerLabel, 0, 0, 2, 1);
	grid.add(head, 0, 1, 1, 1);
        grid.add(submit, 0, 2, 1, 1);
        
        submit.setOnAction(e -> {
            if(head.getText().length() > 0){
                header = head.getText();
                this.hide();
            }
            else{
                head.setPromptText("Enter Header Text Here");
            }
        });
        
        headerLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene scene = new Scene(grid);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        setScene(scene);
    
    }
    public String getHeader(){return header;}
}
