/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;
import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_ADD_SLIDE;
import static epg.StartupConstants.ICON_MOVE_DOWN;
import static epg.StartupConstants.ICON_MOVE_UP;
import static epg.StartupConstants.ICON_REMOVE_SLIDE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.slideshow.Slide;
import epg.slideshow.SlideEditView;
import epg.slideshow.SlideShowModel;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class SlideshowComponentDialog extends Stage{
    
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;
    
    // WORKSPACE
    HBox workspace;

    //BUTTON TO SUBMIT SLIDESHOW
    Button submitBtn;
    
    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;
    
    //DATA FOR SLIDE COMPONENT
    SlideShowModel slides;
    ArrayList<Slide> slideData;
    SlideShowModel tempSlides;
    Slide selectedSlide;
    
    public SlideshowComponentDialog(){
        slides = new SlideShowModel(this);
        slideData = new ArrayList<>();
        tempSlides = new SlideShowModel(this);
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();
    
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        initWindow("Add a Slideshow");
        
        //SET UP EVENT HANDLERS
        initEventHandlers();
        
        
    }
    
    public SlideshowComponentDialog(Component c){
        slides = new SlideShowModel(this);
        slideData = new ArrayList<>();
        tempSlides = new SlideShowModel(this);
        tempSlides.setSlides(c.getSlides());
        
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();
    
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        initWindow("Add a Slideshow");
        
        //SET UP EVENT HANDLERS
        initEventHandlers();
        
        reloadSlideShowPane();
        
        
    }
    
    
    //RELOAD PANE TO REFLECT UPDATES
    public void reloadSlideShowPane(){
        slidesEditorPane.getChildren().clear();
        if(tempSlides.getSelectedSlide() != null){
            removeSlideButton.setDisable(false);
            moveSlideUpButton.setDisable(false);
            moveSlideDownButton.setDisable(false);
            
            if(tempSlides.getSelectedSlide().equals(tempSlides.getSlides().get(0))){
            moveSlideUpButton.setDisable(true);
        }
        else{
            moveSlideUpButton.setDisable(false);
        }
        if(tempSlides.getSelectedSlide().equals(tempSlides.getSlides().get(tempSlides.getSlides().size() - 1))){
            moveSlideDownButton.setDisable(true);
        }
        else{
            moveSlideDownButton.setDisable(false);
        }
        }
        else{
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
            moveSlideDownButton.setDisable(true);
        }
        
	for (Slide slide : tempSlides.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
	    if (tempSlides.isSelectedSlide(slide))
		slideEditor.setStyle("-fx-background-color: red");
	    else
		slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	    slidesEditorPane.getChildren().add(slideEditor);
	    slideEditor.setOnMousePressed(e -> {
		tempSlides.setSelectedSlide(slide);
		this.reloadSlideShowPane();
	    });
	}
        slidesEditorPane.setStyle("-fx-background-color: rgb(255, 255, 133)");
        slidesEditorPane.setStyle("-fx-border-color: rgb(0, 0, 10)");
        slidesEditorPane.setStyle("-fx-border-thickness: 10px");
    }
    
    //INIT EVENT HANDLERS
    private void initEventHandlers(){
        
        addSlideButton.setOnAction(e->{
            tempSlides.addSlide("DefaultStartSlide.png", PATH_ICONS, "Caption");
        });
        removeSlideButton.setOnAction(e->{
            tempSlides.removeSelectedSlide();
            
        });
        moveSlideUpButton.setOnAction(e->{
            tempSlides.moveSelectedSlideUp();
            
        });
        moveSlideDownButton.setOnAction(e->{
            tempSlides.moveSelectedSlideDown();
        });
        submitBtn.setOnAction(e->{
            if(tempSlides.getSelectedSlide() != null){
                slides = tempSlides;
            }
            this.hide();
        });
    }
    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        workspace.setStyle("-fx-background-color: rgb(255, 255, 133)");
        
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox(10);
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    "Add Slide",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
	removeSlideButton = initChildButton(slideEditToolbar,	ICON_REMOVE_SLIDE,  "Remove Slide",   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	moveSlideUpButton = initChildButton(slideEditToolbar,	ICON_MOVE_UP,	    "Move Up",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	moveSlideDownButton = initChildButton(slideEditToolbar,	ICON_MOVE_DOWN,	    "Move Down",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	
        submitBtn = new Button("SUBMIT");
        submitBtn.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
        slidesEditorPane.setStyle("-fx-background-color: rgb(255, 255, 133)");
        slidesEditorPane.setStyle("-fx-border-color: rgb(0, 0, 10)");
        slidesEditorPane.setStyle("-fx-border-thickness: 10px");
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        slidesEditorScrollPane.setStyle("-fx-border-color: rgb(255, 255, 133)");
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
        workspace.getChildren().add(slidesEditorPane);
    }
    
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	this.setTitle(windowTitle);
       

	// AND USE IT TO SIZE THE WINDOW
	this.setWidth(600);
	this.setHeight(600);

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
        ssmPane.setCenter(workspace);
        ssmPane.setBottom(submitBtn);
        ssmPane.setStyle("-fx-background-color: #B9D9C3");
	ssmPane.setStyle("-fx-font-family: Georgia, serif");
        primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	this.setScene(primaryScene);
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    public SlideShowModel getSlideshow(){return slides;}
}
