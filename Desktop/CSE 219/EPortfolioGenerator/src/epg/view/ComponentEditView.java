/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.TOOLTIP_HYPERLINK;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SITE_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_EDIT_COMPONENT;
import static epg.StartupConstants.ICON_HYPERLINK_COMPONENT;
import static epg.StartupConstants.ICON_REMOVE_SLIDE;
import static epg.StartupConstants.PATH_ICONS;
import epg.model.Component;
import epg.model.Page;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class ComponentEditView extends GridPane{
    // SLIDE THIS COMPONENT EDITS
    Page page;
    
    // CONTROLS FOR EDITING THE PAGE NAME
    HBox titleHBox;
    Label titleLabel;
    VBox btnContainer;
    Button removeBtn;
    Button editBtn;
    Component component;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initComp The slide to be edited by this component.
     * @param selectedPage Page we are on
     * @param view workspace
     */
    public ComponentEditView(Component initComp, Page selectedPage, WorkspaceView view){
	
	
	
	component = initComp;

	// SETUP THE TITLE CONTROLS
	titleHBox = new HBox();
        
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	titleHBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
	titleLabel = new Label(component.getTitle());
	titleHBox.getChildren().addAll(titleLabel);
        
        //ADD BUTTONS TO CONTAINER
        btnContainer = new VBox();
        removeBtn = new Button();
	String imagePath = "file:" + PATH_ICONS + ICON_REMOVE_SLIDE;
	Image buttonImage = new Image(imagePath);
	removeBtn.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip("Remove Component");
	removeBtn.setTooltip(buttonTooltip);
        editBtn = new Button();
	String imagePathEdit = "file:" + PATH_ICONS + ICON_EDIT_COMPONENT;
	Image buttonImageEdit = new Image(imagePathEdit);
	editBtn.setGraphic(new ImageView(buttonImageEdit));
	Tooltip buttonTooltipEdit = new Tooltip("Edit Component");
	editBtn.setTooltip(buttonTooltipEdit);
        
        editBtn.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR_BUTTON);
        editBtn.setStyle("-fx-background-color: gray");
        removeBtn.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR_BUTTON);
        removeBtn.setStyle("-fx-background-color: gray");
        
        btnContainer.getChildren().addAll(removeBtn, editBtn);
        
        //INIT CONTROLS
        removeBtn.setOnAction(e->{
            selectedPage.removeComponent();
            view.reloadSitePane();
        });
        
        editBtn.setOnAction(e->{
            String type = selectedPage.getSelectedComponent().getTitle();
            switch(type){
                case"Paragraph":
                    ParagraphComponentDialog pD= new ParagraphComponentDialog(selectedPage.getSelectedComponent());
                    pD.showAndWait();
                    selectedPage.getSelectedComponent().setParagraph(pD.getParagraph());
                    selectedPage.getSelectedComponent().setFont(pD.getFont());
                    break;
                case"List":
                    ListComponentDialog lD = new ListComponentDialog(selectedPage.getSelectedComponent());
                    lD.showAndWait();
                    selectedPage.getSelectedComponent().setList(lD.getList());
                    break;
                case"Header":
                    HeaderComponentDialog hD = new HeaderComponentDialog(selectedPage.getSelectedComponent());
                    hD.showAndWait();
                    selectedPage.getSelectedComponent().setHeader(hD.getHeader());
                    break;
                case"Slide Show":
                    SlideshowComponentDialog sD = new SlideshowComponentDialog(selectedPage.getSelectedComponent());
                    sD.showAndWait();
                    selectedPage.getSelectedComponent().setSlides(sD.getSlideshow().getSlides());
                    break;
                case"Image":
                    ImageComponentDialog iD = new ImageComponentDialog(selectedPage.getSelectedComponent());
                    iD.showAndWait();
                    selectedPage.getSelectedComponent().setImageFileName(iD.getImageFileName());
                    selectedPage.getSelectedComponent().setCaption(iD.getCaption());
                    selectedPage.getSelectedComponent().setFloatPref(iD.getFloatPref());
                    selectedPage.getSelectedComponent().setWidth(iD.getImageWidth());
                    selectedPage.getSelectedComponent().setHeight(iD.getImageHeight());
                    break;
                case"Video":
                    VideoComponentDialog vD = new VideoComponentDialog(selectedPage.getSelectedComponent());
                    vD.showAndWait();
                    selectedPage.getSelectedComponent().setVideoFileName(vD.getVideoFileName());
                    selectedPage.getSelectedComponent().setVideoCaption(vD.getVideoCaption());
                    selectedPage.getSelectedComponent().setVideoWidth(vD.getVideoWidth());
                    selectedPage.getSelectedComponent().setVideoHeight(vD.getVideoHeight());
                            
                    break;
                default:
                    break;
            }
        });

	// LAY EVERYTHING OUT INSIDE THIS COMPONENt
        this.setPrefWidth(300);
        this.add(titleHBox, 0, 0);
        this.add(btnContainer, 2, 0);
        
    }   
        
        
        //METHODS TO ACCESS BUTTONS IN GUI CLASS
        public Button getRemoveBtn(){return removeBtn;}
        public Button getEditBtn(){return editBtn;}
}
