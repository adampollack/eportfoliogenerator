/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import epg.model.Page;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class PageEditView extends HBox{
   // SLIDE THIS COMPONENT EDITS
    Page page;
    
    // CONTROLS FOR EDITING THE PAGE NAME
    VBox titleVBox;
    Label titleLabel;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initPage The slide to be edited by this component.
     */
    public PageEditView(Page initPage) {
	
	
	// KEEP THE PAGE FOR LATER
	page = initPage;

	// SETUP THE TITLE CONTROLS
	titleVBox = new VBox();
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	titleVBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
	titleLabel = new Label(page.getTitle());
	titleVBox.getChildren().add(titleLabel);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENt
	getChildren().add(titleVBox);

	
    }   
     
     
}
