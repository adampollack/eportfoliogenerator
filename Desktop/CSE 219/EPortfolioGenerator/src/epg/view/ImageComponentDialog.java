/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.StartupConstants.CSS_CLASS_COMP_COMBO_BOX;
import static epg.StartupConstants.CSS_CLASS_COMP_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_COMP_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LIST_LABEL;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static epg.StartupConstants.LABEL_IMAGE_COMPONENT_DIALOG;
import static epg.StartupConstants.OK_BUTTON_TEXT;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.error.ErrorHandler;
import static epg.file.PortfolioFileManager.SLASH;
import epg.model.Component;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class ImageComponentDialog extends Stage{
    GridPane gridPane;
    Label dialogLabel;
    Label captionLabel;
    Label imageFileLabel;
    Label widthLabel;
    Label heightLabel;
    Label floatLabel;
    ComboBox<String> floatComboBox;
    Button submitButton;
    Button imageBrowseBtn;
    ImageView imageview;
    String fileName;
    String path;
    String tempFileName;
    String tempPath;
    String caption;
    int imageHeight;
    int imageWidth;
    TextField imgFileTextField;
    TextField captionTextField;
    TextField imageWidthTextField;
    TextField imageHeightTextField;
    ObservableList<String> floatChoices;
    String floatSelection;
    FileChooser imageFileChooser;
    
    public ImageComponentDialog(){
        this.setTitle(LABEL_IMAGE_COMPONENT_DIALOG);
        
        dialogLabel = new Label(LABEL_IMAGE_COMPONENT_DIALOG + ":");
        
        imageFileLabel = new Label("Select an Image File:");
        imageFileLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        captionLabel = new Label("Enter a Caption:");
        captionLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        widthLabel = new Label("Enter Image Width:");
        widthLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        heightLabel = new Label("Enter Image Height:");
        heightLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        floatLabel = new Label("Select Float Property:");
        floatLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        
        captionTextField = new TextField();
        imageWidthTextField = new TextField();
        imageHeightTextField = new TextField();
        imgFileTextField = new TextField();
        
        
        
        imageBrowseBtn = new Button("BROWSE");
        imageBrowseBtn.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        imageview = new ImageView();
        imageview.setId("yes");
        
        imageFileChooser = new FileChooser();
        // SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        
        imageBrowseBtn.setOnMouseClicked(e ->{
            // LET'S OPEN THE FILE CHOOSER
            File file = imageFileChooser.showOpenDialog(null);
            if (file != null) {
                tempPath = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                tempFileName = file.getName();
            
                String imagePath = tempPath + SLASH + tempFileName;
                File imageFile = new File(imagePath);
                imgFileTextField.setText(imagePath);
                
                // GET AND SET THE IMAGE
                URL fileURL;
                try {
                    fileURL = imageFile.toURI().toURL();
                
                Image slideImage = new Image(fileURL.toExternalForm());
                imageview.setImage(slideImage);
                imageview.setId("yes");
	    
                // AND RESIZE IT
                double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
                double perc = scaledWidth / slideImage.getWidth();
                double scaledHeight = slideImage.getHeight() * perc;
                imageview.setFitWidth(scaledWidth);
                imageview.setFitHeight(scaledHeight);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
       
        // INIT THE CHOICES
	ObservableList<String> floatChoices = FXCollections.observableArrayList();
	floatChoices.add("Right");
	floatChoices.add("Left");
        floatChoices.add("None");
	floatComboBox = new ComboBox(floatChoices);
	submitButton = new Button("SUBMIT");
        submitButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        
        //EVENT HANDLER FOR IMAGE DIMENSIONS
        submitButton.setOnAction(e ->{
            int imgWidth = 0;
            int imgHeight = 0;
            try{
                String text = imageWidthTextField.getText();
                imgWidth = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
            }
            try{
                String text = imageHeightTextField.getText();
                imgHeight = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
            }
            
            String text = captionTextField.getText();
            if(text.isEmpty())
                captionTextField.setText("Must Enter Caption");
            else
                caption = text;
            
            
            if(imgWidth > 0 && imgWidth <= 1000 && imgHeight > 0 && imgHeight <= 1000 && 
                    !caption.isEmpty() && tempFileName != null && (tempFileName.substring(tempFileName.length() - 3).equals("png") ||
                    tempFileName.substring(tempFileName.length() - 3).equals("jpg") 
                    || tempFileName.substring(tempFileName.length() - 3).equals("gif"))){
                imageWidth = imgWidth;
                imageHeight = imgHeight;
                fileName = tempFileName;
                path = tempPath;
                this.hide();
            }
            else{
                imageWidthTextField.setText("Enter a value from 1 to 1000");
                imageHeightTextField.setText("Enter a value from 1 to 1000");
                captionTextField.setText("Must Enter Caption");
                imgFileTextField.setText("File Extensions: .png .jpg or .gif");
                imageview.setId("no");
            }
        });
	
	gridPane = new GridPane();
	gridPane.add(imageFileLabel, 0, 0);
        gridPane.add(imgFileTextField, 1, 0);
	gridPane.add(imageBrowseBtn, 2, 0);
        if(!imageview.getId().equals("no"))
            gridPane.add(imageview, 3, 0);
        gridPane.add(captionLabel, 0, 1);
        gridPane.add(captionTextField, 1, 1);
        gridPane.add(widthLabel, 0, 2);
        gridPane.add(imageWidthTextField, 1, 2);
        gridPane.add(heightLabel, 0, 3);
        gridPane.add(imageHeightTextField, 1, 3);
        gridPane.add(floatLabel, 0, 4);
        gridPane.add(floatComboBox, 1, 4);
        gridPane.add(submitButton, 0, 5);
	
	
	
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_IMAGE_DIALOG_PANE);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        
    }
    
    public ImageComponentDialog(Component comp){
        this.setTitle("EDIT IMAGE");
        
        dialogLabel = new Label(LABEL_IMAGE_COMPONENT_DIALOG + ":");
        
        imageFileLabel = new Label("Select an Image File:");
        imageFileLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        captionLabel = new Label("Enter a Caption:");
        captionLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        widthLabel = new Label("Enter Image Width:");
        widthLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        heightLabel = new Label("Enter Image Height:");
        heightLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        floatLabel = new Label("Select Float Property:");
        floatLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        
        captionTextField = new TextField();
        captionTextField.setText(comp.getCaption());
        imageWidthTextField = new TextField();
        imageWidthTextField.setText(String.valueOf(comp.getWidth()));
        imageHeightTextField = new TextField();
        imageHeightTextField.setText(String.valueOf(comp.getHeight()));
        imgFileTextField = new TextField();
        
            
       String imagePathe = comp.getImagePath() + comp.getImageFileName();
       tempPath = comp.getImagePath();
       tempFileName = comp.getImageFileName();
       imgFileTextField.setText(imagePathe);
       imageview = new ImageView();
       File initFile = new File(imagePathe);
       URL initFileURL;
                try {
                    initFileURL = initFile.toURI().toURL();
                
                Image image = new Image(initFileURL.toExternalForm());
                imageview.setImage(image);
                imageview.setId("yes");
	    
                // AND RESIZE IT
                double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
                double perc = scaledWidth / image.getWidth();
                double scaledHeight = image.getHeight() * perc;
                imageview.setFitWidth(scaledWidth);
                imageview.setFitHeight(scaledHeight);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
                
        imageBrowseBtn = new Button("BROWSE");
        imageBrowseBtn.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        
        imageview.setId("yes");
        
        imageFileChooser = new FileChooser();
        // SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        
        imageBrowseBtn.setOnMouseClicked(e ->{
            // LET'S OPEN THE FILE CHOOSER
            File file = imageFileChooser.showOpenDialog(null);
            if (file != null) {
                tempPath = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                tempFileName = file.getName();
            
                String imagePath = tempPath + SLASH + tempFileName;
                File imageFile = new File(imagePath);
                imgFileTextField.setText(imagePath);
                
                // GET AND SET THE IMAGE
                URL fileURL;
                try {
                    fileURL = imageFile.toURI().toURL();
                
                Image slideImage = new Image(fileURL.toExternalForm());
                imageview.setImage(slideImage);
                imageview.setId("yes");
	    
                // AND RESIZE IT
                double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
                double perc = scaledWidth / slideImage.getWidth();
                double scaledHeight = slideImage.getHeight() * perc;
                imageview.setFitWidth(scaledWidth);
                imageview.setFitHeight(scaledHeight);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
       
        // INIT THE CHOICES
	ObservableList<String> floatChoices = FXCollections.observableArrayList();
	floatChoices.add("Right");
	floatChoices.add("Left");
        floatChoices.add("None");
	floatComboBox = new ComboBox(floatChoices);
        floatComboBox.setValue(comp.getFloatPref());
	submitButton = new Button("SUBMIT");
        submitButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        
        //EVENT HANDLER FOR IMAGE DIMENSIONS
        submitButton.setOnAction(e ->{
            int imgWidth = 0;
            int imgHeight = 0;
            try{
                String text = imageWidthTextField.getText();
                imgWidth = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
            }
            try{
                String text = imageHeightTextField.getText();
                imgHeight = Integer.parseInt(text);
            }
            catch(NumberFormatException ex){
            }
            
            String text = captionTextField.getText();
            if(text.isEmpty())
                captionTextField.setText("Must Enter Caption");
            else
                caption = text;
            
            
            if(imgWidth > 0 && imgWidth <= 1000 && imgHeight > 0 && imgHeight <= 1000 && 
                    !caption.isEmpty() && tempFileName != null && (tempFileName.substring(tempFileName.length() - 3).equals("png") ||
                    tempFileName.substring(tempFileName.length() - 3).equals("jpg") 
                    || tempFileName.substring(tempFileName.length() - 3).equals("gif"))){
                imageWidth = imgWidth;
                imageHeight = imgHeight;
                fileName = tempFileName;
                path = tempPath;
                this.hide();
            }
            else{
                imageWidthTextField.setText("Enter a value from 1 to 1000");
                imageHeightTextField.setText("Enter a value from 1 to 1000");
                captionTextField.setText("Must Enter Caption");
                imgFileTextField.setText("File Extensions: .png .jpg or .gif");
                imageview.setId("no");
            }
        });
	
	gridPane = new GridPane();
	gridPane.add(imageFileLabel, 0, 0);
        gridPane.add(imgFileTextField, 1, 0);
	gridPane.add(imageBrowseBtn, 2, 0);
        if(!imageview.getId().equals("no"))
            gridPane.add(imageview, 3, 0);
        gridPane.add(captionLabel, 0, 1);
        gridPane.add(captionTextField, 1, 1);
        gridPane.add(widthLabel, 0, 2);
        gridPane.add(imageWidthTextField, 1, 2);
        gridPane.add(heightLabel, 0, 3);
        gridPane.add(imageHeightTextField, 1, 3);
        gridPane.add(floatLabel, 0, 4);
        gridPane.add(floatComboBox, 1, 4);
        gridPane.add(submitButton, 0, 5);
	
	
	
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_IMAGE_DIALOG_PANE);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        
    }
    
    public String getImagePath(){return path;}
    public String getImageFileName(){return fileName;}
    public String getFloatPref(){return floatComboBox.getValue();}
    public int getImageWidth(){return imageWidth;}
    public int getImageHeight() {return imageHeight;}
    public String getCaption(){return caption;}
    
    
    
}
