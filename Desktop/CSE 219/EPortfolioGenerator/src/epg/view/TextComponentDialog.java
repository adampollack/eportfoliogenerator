/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.LanguagePropertyType.TOOLTIP_HYPERLINK;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_COMP_COMBO_BOX;
import static epg.StartupConstants.CSS_CLASS_COMP_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_COMP_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_COMP_SUBMIT_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LIST_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LIST_LABEL;
import static epg.StartupConstants.CSS_CLASS_TEXT_PROMPT;
import static epg.StartupConstants.ICON_HYPERLINK_COMPONENT;
import static epg.StartupConstants.LABEL_TEXT_COMPONENT_DIALOG;
import static epg.StartupConstants.OK_BUTTON_TEXT;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Text;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Adam
 */
public class TextComponentDialog extends Stage{
    GridPane gridPane;
    Label textPromptLabel;
    ComboBox textTypeComboBox;
    Button okButton;
    String selectedType;
    String paragraph;
    ObservableList<String> listItems;
    
    public TextComponentDialog(){
        //WINDOW TITLE
        this.setTitle(LABEL_TEXT_COMPONENT_DIALOG);
        
        //PROMPT FOR COMBO BOX
        textPromptLabel = new Label(LABEL_TEXT_COMPONENT_DIALOG);
        
        // INIT THE CHOICES
	ObservableList<String> compChoices = FXCollections.observableArrayList();
	compChoices.add("Paragraph");
	compChoices.add("List");
        compChoices.add("Header");
	textTypeComboBox = new ComboBox(compChoices);
	okButton = new Button(OK_BUTTON_TEXT);
	
	gridPane = new GridPane();
	gridPane.add(textPromptLabel, 0, 0, 2, 1);
	gridPane.add(textTypeComboBox, 0, 1, 1, 1);
	gridPane.add(okButton, 1, 1, 1, 1);
	
	okButton.setOnAction(e -> {
            this.hide();
	    selectedType = textTypeComboBox.getSelectionModel().getSelectedItem().toString();
            
	});
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_COMP_DIALOG_PANE);
        textPromptLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
	textTypeComboBox.getStyleClass().add(CSS_CLASS_COMP_COMBO_BOX);
	okButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
    }
    
    public String getSelectedType(){
        return selectedType;
    }
    
    
    
    /*NEED TO ADD BUTTONS FOR REMOVING HYPERLINS AND EDITING URL. 
    NEED TO CHANGE THE TEXT VISUALLY THAT IS A HYPERLINK.
    NEED TO STORE THE HIGHLIGHTED TEXT FOR THE COMPONENT. 
    IDEA I HAVE IS TO EXTRACT HIGHLIGHTED SUBSTRING THAT IS THE HYPERINK
    FROM THE FOOTER STRING. IF THERE ARE MULTIPLE HYPERLINKS, THESE EXTRACTED
    STRINGS CAN BE STORED IN A DOUBLE DATA ARRAY. THE FIRST DATA IS THE SUBSTRING
    AND THE SECOND IS A BOOLEAN FLAG FOR WHETHER OR NOT IT IS HIGHLIGHTED TEXT.
    THIS CAN BE APPLIED FOR ALL HYPERLINK TEXT NEEDS.
    */
   public static Text hyperlinkDialog(Text word){
        Stage stage = new Stage();
        stage.setTitle("Add Hyperlink URL");
        
        TextField header = new TextField();
        header.setStyle("-fx-font-size: 14pt");
        header.setEditable(true);
        
        Label headerLabel = new Label("Enter the URL:");
        
        Button submit = new Button("SUBMIT");
	
	GridPane grid = new GridPane();
        grid.setVgap(5);
	grid.add(headerLabel, 0, 0, 2, 1);
	grid.add(header, 0, 1, 1, 1);
        grid.add(submit, 0, 2, 1, 1);
        
        submit.setOnAction(e -> {
            if(header.getText() != null){
                word.makeHyperlink(header.getText());
                stage.hide();
            }
            else
                header.setText("Enter a URL");
        });
        
        headerLabel.getStyleClass().add(CSS_CLASS_TEXT_PROMPT);
        submit.getStyleClass().add(CSS_CLASS_COMP_SUBMIT_BUTTON);
        
        Scene headerDialog = new Scene(grid);
        
        headerDialog.getStylesheets().add(STYLE_SHEET_UI);
        stage.setScene(headerDialog);
        stage.show();
        
        return word;
    }
    
   public String getParagraph(){return paragraph;}
}
