/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_COMP_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LIST_LABEL;
import static epg.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static epg.StartupConstants.LABEL_IMAGE_COMPONENT_DIALOG;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.file.PortfolioFileManager.SLASH;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class BannerImageDialog extends Stage{
    GridPane gridPane;
    Label dialogLabel;
    Label imageFileLabel;
    Button submitButton;
    Button imageBrowseBtn;
    ImageView imageview;
    String fileName;
    String path;
    FileChooser imageFileChooser;
    
    public BannerImageDialog(){
        this.setTitle(LABEL_IMAGE_COMPONENT_DIALOG);
        
        dialogLabel = new Label(LABEL_IMAGE_COMPONENT_DIALOG + ":");
        
        imageFileLabel = new Label("Select an Image File:");
        imageFileLabel.getStyleClass().add(CSS_CLASS_LIST_LABEL);
        
        imageBrowseBtn = new Button("BROWSE");
        imageBrowseBtn.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
        imageview = new ImageView();
        
        imageFileChooser = new FileChooser();
        // SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        
        imageBrowseBtn.setOnMouseClicked(e ->{
            // LET'S OPEN THE FILE CHOOSER
            File file = imageFileChooser.showOpenDialog(null);
            if (file != null) {
                path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                fileName = file.getName();
            
                String imagePath = path + SLASH + fileName;
                File imageFile = new File(imagePath);
                
                // GET AND SET THE IMAGE
                URL fileURL;
                try {
                    fileURL = imageFile.toURI().toURL();
                
                Image slideImage = new Image(fileURL.toExternalForm());
                imageview.setImage(slideImage);
	    
                // AND RESIZE IT
                double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
                double perc = scaledWidth / slideImage.getWidth();
                double scaledHeight = slideImage.getHeight() * perc;
                imageview.setFitWidth(scaledWidth);
                imageview.setFitHeight(scaledHeight);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ImageComponentDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
       
        
	submitButton = new Button("SUBMIT");
        submitButton.getStyleClass().add(CSS_CLASS_COMP_OK_BUTTON);
	
	gridPane = new GridPane();
	gridPane.add(imageFileLabel, 0, 0);
	gridPane.add(imageBrowseBtn, 1, 0);
	gridPane.add(imageview, 2, 0);
        gridPane.add(submitButton, 0, 1);
	
	submitButton.setOnMouseClicked(e -> {
	    this.hide();
        });
	
	
	// SPECIFY STYLE CLASSES
	gridPane.getStyleClass().add(CSS_CLASS_IMAGE_DIALOG_PANE);
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gridPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        
    }
    public String getBannerImage(){
        return fileName;
    }

}
