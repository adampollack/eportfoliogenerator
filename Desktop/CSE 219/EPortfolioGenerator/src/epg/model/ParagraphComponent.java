/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.view.TextComponentDialog;
import java.util.ArrayList;

/**
 *
 * @author Adam
 */
public class ParagraphComponent extends Component {
    private String title;
    private String paragraph;
    private String font;
    
    public ParagraphComponent(String par, String font){
        title = "Paragraph";
        paragraph = par;
        this.font = font;
    }
    
    public String getParagraph(){return paragraph;}
    public String getTitle(){return title;}
    public String getFont(){return font;}
    
    public void setParagraph(String par){
        paragraph = par;
    }
    
    public void setFont(String font){
        this.font = font;
    }
}
