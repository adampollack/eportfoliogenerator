/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.slideshow.Slide;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author Adam
 */
public abstract class Component {
    private String imageFileName;
    private String floatPref;
    private String caption;
    private int width;
    private int height;
    private String imagePath;
    
    private String videoFileName;
    private String videoPath;
    private String videoCaption;
    private int videoWidth;
    private int videoHeight;
    
    private String paragraph;
    private String font;
    
    private String header;
    
    private ArrayList<String> list;
    
    private ObservableList<Slide> slides;
    
    private String title;
    
    public Component(){
    }
    
    public String getTitle(){
        return title;
    }
    
    public String getVideoPath(){return videoPath;}
    public String getImageFileName(){return imageFileName;}
    public String getFloatPref(){return floatPref;}
    public String getCaption(){return caption;}
    public int getWidth(){return width;}
    public int getHeight(){return height;}
    public String getVideoFileName(){return videoFileName;}
    public String getVideoCaption(){return videoCaption;}
    public int getVideoWidth(){return videoWidth;}
    public int getVideoHeight(){return videoHeight;}
    public ObservableList<Slide> getSlides(){return slides;}
    public String getParagraph(){return paragraph;}
    public String getFont(){return font;}
    public ArrayList<String> getList(){return list;}
    public String getHeader(){return header;}
    
    public void setVideoPath(String p){
        videoPath = p;
    }
    
    public void setImagePath(String p){
        imagePath = p;
    }
    
    public String getImagePath(){
        return imagePath;
    }
    
    public void setHeader(String h){
        header = h;
    }
    
    public void setList(ArrayList<String> l){
        list = l;
    }
    
    public void setSlides(ObservableList<Slide> s){
        slides = s;
    }
    
    public void setVideoHeight(int h){
        height = h;
    }
    
    public void setVideoWidth(int w){
        width = w;
    }
    
    public void setVideoCaption(String v){
        videoCaption = v;
    }
    
    public void setVideoFileName(String v){
        videoFileName = v;
    }
    
    public void setImageFileName(String n){
        imageFileName = n;
    }
    
    public void setFloatPref(String f){
        floatPref = f;
    }
    
    public void setHeight(int h){
        height = h;
    }
    
    public void setWidth(int w){
        width = w;
    }
    
    public void setCaption(String c){
        caption = c;
    }
    
    public void setParagraph(String p){
        paragraph = p;
    }
    
    public void setFont(String f){
        font = f;
    }
    
    
    
}
