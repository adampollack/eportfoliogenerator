/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.slideshow.SlideComponent;
import epg.slideshow.SlideShowModel;
import epg.view.HeaderComponentDialog;
import epg.view.ImageComponentDialog;
import epg.view.ListComponentDialog;
import epg.view.ParagraphComponentDialog;
import epg.view.SlideshowComponentDialog;
import epg.view.TextComponentDialog;
import epg.view.VideoComponentDialog;
import epg.view.WorkspaceView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Adam
 */
public class Page {
    private WorkspaceView ui;
    private String title;
    private String bannerImageFileName;
    private String imagePath;
    private String font;
    private String layout;
    private String color;
    private String footer;
    private ObservableList<Component> components;
    private Component selectedComponent;
    
     /**
     * Constructor, it initializes all slide data.
     * @param initUI initial ui
     * @param initTitle Title of the page.
     */
    public Page(WorkspaceView initUI, String initTitle) {
        ui = initUI;
	title = initTitle;
        components = FXCollections.observableArrayList();
        font = "default";
        color = "default";
        layout = "default";
    }
    
    public boolean isComponentSelected(){
      return selectedComponent != null;
    }
    
    public boolean isSelectedComponent(Component testComponent){
        return selectedComponent == testComponent;
    }
    
    public ObservableList<Component> getComponents() {
	return components;
    }
    
    public Component getSelectedComponent(){
        return selectedComponent;
    }
    
    public void addTextComponent(TextComponentDialog textData, String textType){
        
        switch(textType.toLowerCase()){
            case "paragraph":
                ParagraphComponentDialog par = new ParagraphComponentDialog();
                par.showAndWait();
                ParagraphComponent compToAdd = new ParagraphComponent(par.getParagraph(), par.getFont());
                if(compToAdd.getParagraph() != null){
                    components.add(compToAdd);
                    selectedComponent = components.get(components.size() - 1);
                }
                ui.reloadComponentEditToolbar();    
                
                break;
                
            case "header":
                HeaderComponentDialog head = new HeaderComponentDialog();
                head.showAndWait();
                HeaderComponent headToAdd = new HeaderComponent(head.getHeader());
                if(headToAdd.getHeader() != null){
                    components.add(headToAdd);
                    selectedComponent = components.get(components.size() - 1);
                }
                ui.reloadComponentEditToolbar();
                break;
                
            case "list":
                ListComponentDialog list = new ListComponentDialog();
                list.showAndWait();
                ListComponent listToAdd = new ListComponent(list.getList());
                if(listToAdd.getList().size() > 0){
                    components.add(listToAdd);
                    selectedComponent = components.get(components.size() - 1);
                }
                ui.reloadComponentEditToolbar();
                break;
        
        }
        
    }
    
    public void addVideoComponent(VideoComponentDialog vidData){
        String vidFileName = vidData.getVideoFileName();
        String vidCaption = vidData.getVideoCaption();
        int width = vidData.getVideoWidth();
        int height = vidData.getVideoHeight();
        VideoComponent compToAdd = new VideoComponent(vidFileName, vidCaption, width, height);
        compToAdd.setVideoPath(vidData.getVideoPath());
        if(!vidFileName.equals("null")){
            components.add(compToAdd);
            selectedComponent = components.get(components.size() - 1);
        }
        ui.reloadComponentEditToolbar();
    }
    
    public void addImageComponent(ImageComponentDialog imageData){
        String imageFileName = imageData.getImageFileName();
        String floatPref = imageData.getFloatPref();
        int width = imageData.getImageWidth();
        int height = imageData.getImageHeight();
        String caption = imageData.getCaption();
        ImageComponent compToAdd = new ImageComponent(imageFileName, floatPref, caption, width, height);
        compToAdd.setImagePath(imageData.getImagePath());
        if(!imageFileName.equals("null")){
            components.add(compToAdd);
            selectedComponent = components.get(components.size() - 1);
        }
        ui.reloadComponentEditToolbar();
    }
    
    public void addSlideComponent(SlideshowComponentDialog slideData){
        SlideShowModel model = slideData.getSlideshow();
        SlideComponent compToAdd = new SlideComponent(model.getSlides());
        if(compToAdd.getSize() > 0){
            components.add(compToAdd);
            selectedComponent = components.get(components.size() - 1);
        }
        ui.reloadComponentEditToolbar();
    }
    
    public void removeComponent(){
        if (isComponentSelected()) {
	    components.remove(selectedComponent);
	    if(components.size() > 0)
                selectedComponent = components.get(0);
	    ui.reloadComponentEditToolbar();
	}
    }
    
     // ACCESSOR METHODS
    public String getTitle() { return title; }
    public String getbannerImageFileName() { return bannerImageFileName; }
    public String getImagePath() { return imagePath; }
    public String getFont() {return font; }
    public String getColor() {return color;}
    public String getLayout() {return layout;}
    public String getFooter() {return footer;}
    
    // MUTATOR METHODS
    public void setComponents(ObservableList<Component> comps){
        components = comps;
    }
    public void setFooter(String footer){
        this.footer = footer;
    }
    
    public void setFont(String font){
        this.font = font;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public void setLayout(String layout){
        this.layout = layout;
    }
    
    public void setbannerImageFileName(String initImageFileName) {
	bannerImageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setBannerImage(String initPath, String initFileName) {
	imagePath = initPath;
	bannerImageFileName = initFileName;
    }
    
    public void setTitle(String initTitle) {
	title = initTitle;
    }
    
    public void setSelectedComponent(Component comp){
        selectedComponent = comp;
    }
    
    
}
