/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.scene.paint.Color;


/**
 *
 * @author Adam
 */
public class Text extends javafx.scene.text.Text{
    private String text;
    private String url;
    private String font;
    private boolean hyperlink;
    
    public Text(String text, String font, boolean hyperlink) {
        this.text = text;
        this.font = font;
        this.hyperlink = hyperlink;
    }
    
    public void makeHyperlink(String url){
        this.setFill(Color.BLUE);
        this.url = url;
    }
    public String getSentenceFont(){return font;}
    public String getSentence(){return text;}
    public boolean isHyperlink(){return hyperlink;}
    
    public void setSentenceFont(String font){
        this.font = font;
    }
    public void setSentence(String text){
        this.text = text;
    }
    
    public void setHyperlink(boolean hyperlink){
        this.hyperlink = hyperlink;
    }
    
    
}
