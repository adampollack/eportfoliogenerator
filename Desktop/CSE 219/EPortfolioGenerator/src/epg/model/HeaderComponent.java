/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Adam
 */
public class HeaderComponent extends Component{
    private String header;
    private String title;
    
    public HeaderComponent(String head){
        title = "Header";
        header = head;
    }
    
    public String getTitle(){return title;}
    public String getHeader(){return header;}
    
    public void setHeader(String h){
        header = h;
    }
    
    
}
