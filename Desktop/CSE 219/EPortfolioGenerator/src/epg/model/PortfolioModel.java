/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import epg.LanguagePropertyType;
import epg.view.WorkspaceView;

/**
 *
 * @author Adam
 */
public class PortfolioModel {
    private WorkspaceView ui; 
    private String title;
    private ObservableList<Page> pages;
    private Page selectedPage;
    private String bannerImageFile;
    
    public PortfolioModel(WorkspaceView initUI) {
        ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();
    }
    
    public String getBannerImageFile(){return bannerImageFile;}
    
    public void setBannerImageFile(String file){
        bannerImageFile = file;
    }
    
    public boolean isPageSelected(){
      return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage){
        return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage(){
        return selectedPage;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void reset(){
        pages.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_EPORTFOLIO_TITLE);
	selectedPage = null;
        bannerImageFile = null;
    }
    
    public void addPage(String initTitle){
        Page pageToAdd = new Page(ui, initTitle);
        pages.add(pageToAdd);
        selectedPage = pages.get(pages.size() - 1);
        ui.reloadSitePane();
        ui.reloadPageEditWorkspace();
       
    }
    
    public void addPage(Page pge){
        pages.add(pge);
        selectedPage = pages.get(pages.size() - 1);
    }
    
    public void removePage(){
        if (isPageSelected()) {
	    pages.remove(selectedPage);
            if(pages.size() > 0)
                this.setSelectedPage(pages.get(0));
            else
                selectedPage = null;
	    ui.reloadSitePane();
	}
        
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public void setSelectedPage(Page newPage){
        selectedPage = newPage;
    }
}
