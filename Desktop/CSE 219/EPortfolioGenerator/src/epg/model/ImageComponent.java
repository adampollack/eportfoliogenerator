/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Adam
 */
public class ImageComponent extends Component{
    private String title;
    private String imageFileName;
    private String imagePath;
    private String floatPref;
    private String caption;
    private int width;
    private int height;
    
    public ImageComponent(String file, String pref, String caption, int width, int height){
        title = "Image";
        imageFileName = file;
        floatPref = pref;
        this.caption = caption;
        this.width = width;
        this.height = height;
    }
    
    public String getImagePath(){
        return imagePath;
    }
    
    public void setImagePath(String p){
        imagePath = p;
    }
    
    public String getImageFileName(){
        return imageFileName;
    }
    
    public String getFloatPref(){ 
        if(floatPref.equals(null))
            return "None";
        return floatPref;
    }
    public String getCaption(){return caption;}
    public int getWidth(){return width;}
    public int getHeight(){return height;}
    public String getTitle(){return title;}
    
    public void setImageFileName(String name){
        imageFileName = name;
    }
    
    public void setFloatPref(String pref){
        floatPref = pref;
    }
    
    public void setWidth(int width){
        this.width = width;
    }
    
    public void setHeight(int height){
        this.height = height;
    }
    
    public void setCaption(String caption){
        this.caption = caption;
    }
}
