/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.util.ArrayList;

/**
 *
 * @author Adam
 */
public class ListComponent extends Component{
    private String title;
    private ArrayList<String> list;
    
    public ListComponent(ArrayList<String> list){
        title = "List";
        this.list = list;
    }
    
    public String getTitle(){return title;}
    public ArrayList<String> getList(){return list;}
    
    public void setList(ArrayList<String> l){
        list = l;
    }
    
}
