/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Adam
 */
public class VideoComponent extends Component{
    private String title;
    private String videoFileName;
    private String videoCaption;
    private String videoPath;
    private int videoWidth;
    private int videoHeight;
    
    public VideoComponent(String file, String caption, int width, int height){
        title = "Video";
        videoFileName = file;
        videoCaption = caption;
        videoWidth = width;
        videoHeight = height;
    }
    
    public String getVideoPath(){
        return videoPath;
    }
    
    public void setVideoPath(String p){
        videoPath = p;
    }
    
    public String getVideoFileName(){
        return videoFileName;
    }
    
    public String getVideoCaption(){return videoCaption;}
    
    public int getVideoWidth(){return videoWidth;}
    public int getVideoHeight(){return videoHeight;}
    public String getTitle(){return title;}
    
    public void setVideoFileName(String name){
        videoFileName = name;
    }
    
    public void setVideoWidth(int width){
        videoWidth = width;
    }
    
    public void setVideoHeight(int height){
        videoHeight = height;
    }
    
    public void setVideoCaption(String v){
        videoCaption = v;
    }
    
}
