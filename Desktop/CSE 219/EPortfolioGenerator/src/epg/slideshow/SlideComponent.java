/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.slideshow;

import epg.model.Component;
import javafx.collections.ObservableList;

/**
 *
 * @author Adam
 */
public class SlideComponent extends Component{
    SlideShowModel model;
    String title;
    ObservableList<Slide> slides;
    
    public SlideComponent(ObservableList<Slide> slds){
        title = "Slide Show";
        slides = slds;
    }
    
    public int getSize(){return slides.size();}
    public String getTitle(){return title;}
    public ObservableList<Slide> getSlides(){return slides;}
    public void setSlides(ObservableList<Slide> s){
        slides = s;
    }
}
