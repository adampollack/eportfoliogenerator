/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import java.io.File;
import javafx.stage.FileChooser;
import epg.StartupConstants;
import epg.model.Page;
import epg.model.PortfolioModel;
import epg.view.ImageComponentDialog;
import epg.view.PageEditView;
import epg.view.SlideshowComponentDialog;
import epg.view.TextComponentDialog;
import epg.view.VideoComponentDialog;
import epg.view.WorkspaceView;

/**
 *
 * @author Adam
 */
public class ComponentSelectionController {
    private WorkspaceView ui;
  
    public ComponentSelectionController(WorkspaceView initUI){
        ui = initUI;
    }
    
    public void processAddText(Page pageToEdit, WorkspaceView view){
        TextComponentDialog textDialog = new TextComponentDialog();
	textDialog.showAndWait();
        pageToEdit.addTextComponent(textDialog, textDialog.getSelectedType());
        ui.reloadSitePane();
    }
    
    public void processSelectImage(Page pageToEdit, WorkspaceView view){
        ImageComponentDialog imageDialog = new ImageComponentDialog();
        imageDialog.showAndWait();
        pageToEdit.addImageComponent(imageDialog);
        ui.reloadSitePane();
    }
    
    public void processSelectVideo(Page pageToEdit, WorkspaceView view){
        VideoComponentDialog videoDialog = new VideoComponentDialog();
        videoDialog.showAndWait();
        pageToEdit.addVideoComponent(videoDialog);
        ui.reloadSitePane();
    }
    
    public void processSelectSlideshow(Page pageToEdit, WorkspaceView view){
        SlideshowComponentDialog slideDialog = new SlideshowComponentDialog();
        slideDialog.showAndWait();
        pageToEdit.addSlideComponent(slideDialog);
        ui.reloadSitePane();
    }
    
    
    
    
    
    
}
