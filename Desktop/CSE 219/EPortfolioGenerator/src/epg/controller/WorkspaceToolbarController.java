/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.model.Page;
import epg.model.PortfolioModel;
import epg.view.BannerImageDialog;
import epg.view.FooterTextDialog;
import epg.view.WorkspaceView;

/**
 *
 * @author Adam
 */
public class WorkspaceToolbarController {
    private WorkspaceView ui;
  
    public WorkspaceToolbarController(WorkspaceView initUI){
        ui = initUI;
    }
    
    public void processBannerImageRequest(PortfolioModel model, WorkspaceView view){
        BannerImageDialog bannerDialog = new BannerImageDialog();
	bannerDialog.showAndWait();
        model.setBannerImageFile(bannerDialog.getBannerImage());
        ui.reloadSitePane();
    }
    
    public void processFooterText(Page pageToEdit, WorkspaceView view){
        FooterTextDialog footerDialog = new FooterTextDialog();
        footerDialog.showAndWait();
        ui.reloadSitePane();
    }
}
