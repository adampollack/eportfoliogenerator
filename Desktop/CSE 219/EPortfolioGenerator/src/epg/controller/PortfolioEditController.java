/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import properties_manager.PropertiesManager;
import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.DEFAULT_PAGE_TITLE;
import epg.StartupConstants;
import epg.model.PortfolioModel;
import epg.view.WorkspaceView;

/**
 *
 * @author Adam
 */
public class PortfolioEditController {
    //APP UI
    private WorkspaceView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PortfolioEditController(WorkspaceView initUI) {
	ui = initUI;
    }
    
    public void processAddPageRequest() {
        PortfolioModel ePortfolio = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	ePortfolio.addPage(props.getProperty(DEFAULT_PAGE_TITLE));
    }
    
    public void processRemovePageRequest(){
        PortfolioModel ePortfolio = ui.getPortfolio();
	ePortfolio.removePage();
	ui.reloadSitePane();
	ui.updateToolbarControls(false);
    }
    
   
}
