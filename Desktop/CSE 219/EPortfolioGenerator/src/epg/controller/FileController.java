/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.stage.FileChooser;
import epg.LanguagePropertyType;
import epg.model.PortfolioModel;
import epg.error.ErrorHandler;
import epg.file.PortfolioFileManager;
import epg.view.WorkspaceView;
import epg.StartupConstants;
import static epg.StartupConstants.PATH_PORTFOLIOS;
import epg.view.SiteViewer;
import epg.view.YesNoCancelDialog;
/**
 *
 * @author Adam
 */
public class FileController {
    //We want to keep track of saving
    private boolean saved;
    
    //The App UI
    private WorkspaceView ui;
    
    //Use this to read and write data
    private PortfolioFileManager portfolioIO;
    
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initPortfolioIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(WorkspaceView initUI, PortfolioFileManager initPortfolioIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        portfolioIO = initPortfolioIO;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    public void handleNewPortfolioRequest() {
        try{
        boolean continueToMakeNew = true;
        if (!saved) {
            // THE USER CAN OPT OUT HERE WITH A CANCEL
            continueToMakeNew = promptToSaveAs();
        }
        if (continueToMakeNew) {
            // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
            PortfolioModel ePortfolio = ui.getPortfolio();
            ePortfolio.reset();
            saved = false;
            
            // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
            
            //THE TAB PANE FOR THE TWO WORKSPACES IS INITIALIZED
            ui.initWorkspaceModeToolbarControls();
            
            //THE SITE TOOLBAR IS INITIALIZED
            
            // MAKE SURE THE TITLE CONTROLS ARE ENABLED
            ui.reloadTitleControls();
            ui.reloadSitePane();
        }
        }catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    public void handleLoadPortfolioRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSaveAs();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING);
        }
    }
    
    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_PORTFOLIOS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		PortfolioModel portfolioToLoad = ui.getPortfolio();
                portfolioIO.loadPortfolio(portfolioToLoad, selectedFile.getAbsolutePath(), ui);
                ui.reloadSitePane();
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
		eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
            }
        }
    }

    public void handleSavePortfolioRequest() {
        try{
        promptToSave();
        }
        catch(IOException e){
            ErrorHandler eH = ui.getErrorHandler();
		eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    public void handleSaveAsPortfolioRequest() {
        try{
            promptToSaveAs();
        }
        catch(IOException e){
            ErrorHandler eH = ui.getErrorHandler();
		eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    public void handleExportPortfolioRequest() {
        
    }

    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSaveAs();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }
    
    private boolean promptToSaveAs() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
	YesNoCancelDialog yesNoCancelDialog = new YesNoCancelDialog(ui.getWindow());
        yesNoCancelDialog.show("SAVE EPORTFOLIO");
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();	
	boolean saveWork = selection.equals(YesNoCancelDialog.YES);

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File(PATH_PORTFOLIOS));
            File file = fileChooser.showSaveDialog(ui.getWindow());
            
            if(file != null){
            PortfolioModel portfolio = ui.getPortfolio();
            portfolioIO.savePortfolioAs(portfolio, file.toString().substring(file.toString().lastIndexOf('\\') + 1));
            saved = true;
            }
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
	YesNoCancelDialog yesNoCancelDialog = new YesNoCancelDialog(ui.getWindow());
        yesNoCancelDialog.show("SAVE EPORTFOLIO");
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();	
	boolean saveWork = selection.equals(YesNoCancelDialog.YES);

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
           
            PortfolioModel portfolio = ui.getPortfolio();
            portfolioIO.savePortfolio(portfolio);
            saved = true;
            
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
    

    
}
