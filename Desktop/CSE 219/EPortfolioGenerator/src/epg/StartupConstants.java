/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

/**
 *
 * @author Adam
 */
public class StartupConstants {
    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_PORTFOLIOS = PATH_DATA + "portfolios/";
    public static String PATH_SITES = "./sites/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_VIDS = "./videos/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_CSS = "file:src/epg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGenerator.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "EPG_Logo.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_SLIDE = "AddSlide.png";
    public static String ICON_REMOVE_SLIDE = "RemoveSlide.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_SLIDE_SHOW_COMPONENT = "SlideShowIcon.png";
    public static String ICON_IMAGE_COMPONENT = "ImageIcon.png";
    public static String ICON_VIDEO_COMPONENT = "VideoIcon.png";
    public static String ICON_TEXT_COMPONENT = "TextIcon.png";
    public static String ICON_ADD_PAGE = "AddPage.png";
    public static String ICON_REMOVE_PAGE = "RemovePage.png";
    public static String ICON_NEW_EPORTFOLIO = "New.png";
    public static String ICON_SAVE_EPORTFOLIO = "Save.png";
    public static String ICON_LOAD_EPORTFOLIO = "Load.png";
    public static String ICON_EXPORT_EPORTFOLIO = "Export.png";
    public static String ICON_SAVE_AS_EPORTFOLIO = "SaveAs.png";
    public static String ICON_BANNER_IMAGE = "BannerImage.png";
    public static String ICON_FOOTER_TEXT = "FooterText.png";
    public static String ICON_EDIT_COMPONENT = "Component.png";
    public static String ICON_HYPERLINK_COMPONENT = "Hyperlink.png";
    
            

    // UI SETTINGS
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_SITE_TOOLBAR = "site_toolbar";
    public static String    CSS_CLASS_STUDENT_NAME_LABEL = "student_name_label";
    public static String    CSS_CLASS_PAGE_TITLE_LABEL = "page_title_label";
    public static String    CSS_CLASS_PAGE_TITLE_TEXT_FIELD = "page_title_text_field";
    public static String    CSS_CLASS_PAGE_EDIT_TOOLBAR_BUTTON = "page_edit_toolbar_button";
    public static String    CSS_CLASS_TOP_WORKSPACE_PANE = "top_workspace_pane";
    public static String    CSS_CLASS_PAGE_EDIT_CONTAINER = "page_edit_container";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW_PANE = "page_edit_view_pane";
    public static String    CSS_CLASS_COMPONENT_PANE = "component_pane";
    public static String    CSS_CLASS_COMPONENT_BUTTONS = "comoponent_buttons";
    public static String    CSS_CLASS_COMPONENT_BUTTON =  "component_button";
    public static String    CSS_CLASS_SITE_TOOLBAR_BUTTON = "site_toolbar_button";
    public static String    CSS_CLASS_SITE_TOOLBAR_CONTAINER = "site_toolbar_container";
    public static String    CSS_CLASS_WORKSPACE_VIEW = "workspace_edit";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW = "page_edit_view";
    public static String    CSS_CLASS_SELECTED_PAGE_EDIT_VIEW = "selected_page_edit_view";
    public static String    CSS_CLASS_FILE_TOOLBAR_BUTTON = "file_toolbar_button";
    public static String    CSS_CLASS_FILE_TOOLBAR_CONTAINER = "file_toolbar_container";
    public static String    CSS_CLASS_STUDENT_NAME_CONTAINER = "student_name_container";
    public static String    CSS_CLASS_WORKSPACE_CONTAINER = "workspace_container";
    public static String    CSS_CLASS_STUDENT_NAME_TEXT = "student_name_text";
    public static String    CSS_CLASS_COMP_DIALOG_PANE = "comp_dialog_pane";
    public static String    CSS_CLASS_TEXT_PROMPT = "text_prompt";
    public static String    CSS_CLASS_COMP_COMBO_BOX = "comp_combo_box";
    public static String    CSS_CLASS_COMP_OK_BUTTON = "comp_ok_button";
    public static String    CSS_CLASS_COMP_SUBMIT_BUTTON = "comp_submit_button";
    public static String    CSS_CLASS_LIST_BUTTON = "list_button";        
    public static String    CSS_CLASS_LIST_LABEL = "list_label";
    public static String    CSS_CLASS_IMAGE_DIALOG_PANE = "image_dialog_pane";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SLIDE_SHOW_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    
    // UI LABELS
    public static String    LABEL_TITLE = "title";
    public static String    LABEL_TEXT_COMPONENT_DIALOG = "Choose a Text Component:";
    public static String    LABEL_IMAGE_COMPONENT_DIALOG = "Choose an Image";
    public static String    LABEL_VID_COMPONENT_DIALOG = "Choose a Video";
    public static String    OK_BUTTON_TEXT = "OK";
}
