/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import epg.StartupConstants;
import static epg.StartupConstants.PATH_PORTFOLIOS;
import epg.model.Component;
import epg.model.HeaderComponent;
import epg.model.ImageComponent;
import epg.model.ListComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import epg.model.PortfolioModel;
import epg.model.VideoComponent;
import epg.slideshow.Slide;
import epg.slideshow.SlideComponent;
import epg.slideshow.SlideShowModel;
import epg.view.WorkspaceView;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Adam
 */
public class PortfolioFileManager {
    // JSON FILE READING AND WRITING CONSTANTS

    public static String JSON_HEADER = "Header";
    public static String JSON_STUDENT_NAME = "student_name";
    public static String JSON_PAGES = "pages";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    
     public void savePortfolio(PortfolioModel portfolioToSave) throws IOException {
	StringWriter sw = new StringWriter();

        //BUILD THE HEADER ARRAY
        JsonObject headerJson = makeHeader(portfolioToSave);
        
	// BUILD THE PAGES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(portfolioToSave.getPages());

	// NOW BUILD THE PORTFOLIO USING EVERYTHING WE'VE ALREADY MADE
	JsonObject portfolioJsonObject = Json.createObjectBuilder()
                .add(JSON_HEADER, headerJson)
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(portfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String portfolioTitle = "" + portfolioToSave.getTitle();
	String jsonFilePath = PATH_PORTFOLIOS + portfolioTitle + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(portfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }
     
     public void savePortfolioAs(PortfolioModel portfolioToSave, String fileName) throws IOException {
	StringWriter sw = new StringWriter();

        //BUILD THE HEADER ARRAY
        JsonObject headerJson = makeHeader(portfolioToSave);
        
	// BUILD THE PAGES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(portfolioToSave.getPages());

	// NOW BUILD THE PORTFOLIO USING EVERYTHING WE'VE ALREADY MADE
	JsonObject portfolioJsonObject = Json.createObjectBuilder()
                .add(JSON_HEADER, headerJson)
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(portfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String jsonFilePath = PATH_PORTFOLIOS + fileName + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(portfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }
     
     private JsonObject makeHeader(PortfolioModel p){
         JsonObjectBuilder jso = Json.createObjectBuilder();
                 jso.add(JSON_STUDENT_NAME, p.getTitle());
                 if(p.getBannerImageFile() != null)
                    jso.add(JSON_IMAGE_FILE_NAME, p.getBannerImageFile());
         
         JsonObject js = jso.build();
         return js;
     }
     
     private JsonArray makePagesJsonArray(List<Page> pages) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
            
	}
	JsonArray jA = jsb.build();
	return jA;
    }
     
     private JsonObject makePageJsonObject(Page page) {
         JsonArrayBuilder jsb = Json.createArrayBuilder();
         for(Component comp: page.getComponents()){
             JsonObject jso = makeComponentJsonObject(comp);
             jsb.add(jso);
         }
            String footer = "";
            String color = "default";
            String layout = "default";
            String font = "default";
            if(page.getFooter() != null){
                footer = page.getFooter();
            }
            if(page.getColor().length() > 0){
                color = page.getColor();
            }
            if(page.getFont().length() > 0){
                font = page.getFont();
            }
            if(page.getLayout().length() > 0){
                layout = page.getLayout();
            }
            JsonObject j = Json.createObjectBuilder()
                    .add("footer", footer)
                    .add("color", color)
                    .add("font", font)
                    .add("layout", layout)
                    .build();
         jsb.add(j);
         JsonArray pges = jsb.build();
	JsonObject pge = Json.createObjectBuilder()
                .add(page.getTitle(), pges)
                .build();
        
        return pge;
         
    }
     
     private JsonObject makeComponentJsonObject(Component component){
         String type = component.getTitle().toLowerCase();
         JsonObjectBuilder compJson = Json.createObjectBuilder();
         JsonObjectBuilder comp = Json.createObjectBuilder();
                 
         
         switch(type){
                case"image":
                    comp.add("image_file_name", component.getImageFileName());
                    comp.add("float", component.getFloatPref());
                    comp.add("caption", component.getCaption());
                    comp.add("height", component.getHeight());
                    comp.add("width", component.getWidth());
                    break;
             
                case"video":
                    comp.add("video_file_name", component.getVideoFileName());
                    comp.add("caption", component.getVideoCaption());
                    comp.add("height", component.getVideoHeight());
                    comp.add("width", component.getVideoWidth());
                    break;
                
                case"slide show":
                    JsonObject slides = makeSlidesJson(component);
                    comp.add("slides", slides);
                    break;
                 
                case"paragraph":
                    comp.add("paragraph", component.getParagraph());
                    comp.add("font", component.getFont());
                    break;
                    
                case"list":
                    JsonObject list = makeListJson(component);
                    comp.add("list", list);
                    break;
                 
                case"header":
                    comp.add("header", component.getHeader());
                    break;
                
                default:
                    break;
         }
         
         JsonObject jso = comp.build();
         compJson.add(component.getTitle(), jso);
         JsonObject js = compJson.build();
         
         return js;
     }
     
     public JsonObject makeSlidesJson(Component comp){
         JsonArrayBuilder jsb = Json.createArrayBuilder();
         for(Slide slide: comp.getSlides()){
             JsonObject jso = Json.createObjectBuilder()
                     .add("image_file_name", slide.getImageFileName())
                     .add("caption", slide.getCaption())
                     .build();
             jsb.add(jso);
         }
         
        JsonArray slides = jsb.build();
	JsonObject slde = Json.createObjectBuilder()
                .add(comp.getTitle(), slides)
                .build();
        
        return slde;
     }
     
     public JsonObject makeListJson(Component comp){
         JsonArrayBuilder jsb = Json.createArrayBuilder();
         for(String str: comp.getList()){
             JsonObject jso = Json.createObjectBuilder()
                     .add("li", str)
                     .build();
             jsb.add(jso);
         }
         JsonArray list = jsb.build();
         JsonObject lst = Json.createObjectBuilder()
                 .add(comp.getTitle(), list)
                 .build();
         return lst;
     }
        
     public void loadPortfolio(PortfolioModel portfolioToLoad, String jsonFilePath, WorkspaceView initui) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
         WorkspaceView ui = initui;
	JsonObject json = loadJSONFile(jsonFilePath);

	// NOW LOAD THE PORTFOLIO
	portfolioToLoad.reset();
        
        //LOAD HEADER AND ADD NAME AND BANNER IMAGE IF THERE IS ONE
        JsonObject jsonHeader = json.getJsonObject("Header");
        portfolioToLoad.setTitle(jsonHeader.getString("student_name"));
        if(jsonHeader.size() > 1)
            portfolioToLoad.setBannerImageFile(jsonHeader.getString("image_file_name"));
        
        //IN PAGES ARRAY ACESS EACH PAGE OBJECT
        //FROM EACH PAGE OBJECT ACCESS EACH COMPONENT
        //DEPENDING ON COMPONENT LOAD DATA DIFFERENTLY
	JsonArray jsonPagesArray = json.getJsonArray("pages");
	for (int i = 0; i < jsonPagesArray.size(); i++) {
            JsonObject pageJso = jsonPagesArray.getJsonObject(i);
            
            Iterator<String> iterator = pageJso.keySet().iterator();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                Page pageToAdd = new Page(ui, key);
                ObservableList<Component> components = FXCollections.observableArrayList();
                String footer = "";
                String color = "";
                String layout = "";
                String font = "";
                JsonArray jA = pageJso.getJsonArray(key);
                
                for(int j = 0; j < jA.size(); j++){
                    JsonObject comp = jA.getJsonObject(j);
                    String type = comp.keySet().toString();
                    switch(type){
                        case"[Paragraph]":
                            JsonObject c = comp.getJsonObject("Paragraph");
                            ParagraphComponent cToAdd = new ParagraphComponent(c.getString("paragraph"), c.getString("font"));
                            components.add(cToAdd);
                            break;
                        case"[Image]":
                            JsonObject img = comp.getJsonObject("Image");
                            ImageComponent iToAdd = new ImageComponent(img.getString("image_file_name"), img.getString("float"), 
                                    img.getString("caption"), img.getInt("width"), img.getInt("height"));
                            components.add(iToAdd);
                            break;
                        case"[Video]":
                            JsonObject vid = comp.getJsonObject("Video");
                            VideoComponent vToAdd = new VideoComponent(vid.getString("video_file_name"), vid.getString("caption"),
                                    vid.getInt("width"), vid.getInt("height"));
                            components.add(vToAdd);
                            break;
                        case"[Slide Show]":
                            ObservableList<Slide> slides = FXCollections.observableArrayList();
                            JsonObject s = comp.getJsonObject("Slide Show");
                            JsonObject ss = s.getJsonObject("slides");
                            JsonArray sldes = ss.getJsonArray("Slide Show");
                            for(int k = 0; k < sldes.size(); k++){
                                JsonObject slde = sldes.getJsonObject(k);
                                Slide sld = new Slide(slde.getString("image_file_name"), slde.getString("caption"));
                                slides.add(sld);
                            }
                            SlideComponent sToAdd = new SlideComponent(slides);
                            components.add(sToAdd);
                            break;
                        case"[List]":
                            ArrayList<String> list = new ArrayList<>();
                            JsonObject l = comp.getJsonObject("List");
                            JsonObject ll = l.getJsonObject("list");
                            JsonArray lst = ll.getJsonArray("List");
                            for(int x = 0; x < lst.size(); x++){
                                JsonObject lt = lst.getJsonObject(x);
                                list.add(lt.getString("li"));
                            }
                            ListComponent lToAdd = new ListComponent(list);
                            components.add(lToAdd);
                            break;
                        case"[Header]":
                            JsonObject h = comp.getJsonObject("Header");
                            HeaderComponent hToAdd = new HeaderComponent(h.getString("header"));
                            components.add(hToAdd);
                            break;
                        default:
                            font = comp.getString("font");
                            color = comp.getString("color");
                            layout = comp.getString("layout");
                            footer = comp.getString("footer");
                            break;
                    }
                }
                
                pageToAdd.setColor(color);
                pageToAdd.setLayout(layout);
                pageToAdd.setFont(font);
                pageToAdd.setFooter(footer);
                pageToAdd.setComponents(components);
                portfolioToLoad.addPage(pageToAdd); 
            }
                  
	}
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
     
   
}
