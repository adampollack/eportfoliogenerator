package ssm.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_SLIDE_SHOW_HEIGHT;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.LABEL_SLIDE_SHOW_TITLE;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.JSON_CAPTION;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_PATH;
import static ssm.file.SlideShowFileManager.JSON_SLIDES;
import static ssm.file.SlideShowFileManager.JSON_TITLE;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class provides the UI for the slide show viewer, note that this class is
 * a window and contains all controls inside.
 *
 * @author McKilla Gorilla & Adam Pollack
 */
public class SlideShowViewer extends Stage {

    // THE MAIN UI

    SlideShowMakerView parentView;

    // THE DATA FOR THIS SLIDE SHOW
    SlideShowModel slides;

    // HERE ARE OUR UI CONTROLS
    BorderPane borderPane;
    FlowPane topPane;
    Label slideShowTitleLabel;
    ImageView slideShowImageView;
    VBox bottomPane;
    Label captionLabel;
    FlowPane navigationPane;
    Button previousButton;
    Button nextButton;

    /**
     * This constructor just initializes the parent and slides references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public SlideShowViewer(SlideShowMakerView initParentView) {
	// KEEP THIS FOR LATER
	parentView = initParentView;

	// GET THE SLIDES
	slides = parentView.getSlideShow();
    }

    /**
     * This method initializes the UI controls and opens the window with the
     * first slide in the slideshow displayed.
     */
    public void startSlideShow() {
	// FIRST THE TOP PANE
	topPane = new FlowPane();
	topPane.setAlignment(Pos.CENTER);
	slideShowTitleLabel = new Label(slides.getTitle());
	slideShowTitleLabel.getStyleClass().add(LABEL_SLIDE_SHOW_TITLE);
	topPane.getChildren().add(slideShowTitleLabel);

	// THEN THE CENTER, START WITH THE FIRST IMAGE
	slideShowImageView = new ImageView();
	reloadSlideShowImageView();

	// THEN THE BOTTOM PANE
	bottomPane = new VBox();
	bottomPane.setAlignment(Pos.CENTER);
	captionLabel = new Label();
	if (slides.getSlides().size() > 0) {
	    captionLabel.setText(slides.getSelectedSlide().getCaption());
	}
	navigationPane = new FlowPane();
	bottomPane.getChildren().add(captionLabel);
	bottomPane.getChildren().add(navigationPane);

	// NOW SETUP THE CONTENTS OF THE NAVIGATION PANE
	navigationPane.setAlignment(Pos.CENTER);
	previousButton = parentView.initChildButton(navigationPane, ICON_PREVIOUS, LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	nextButton = parentView.initChildButton(navigationPane, ICON_NEXT, LanguagePropertyType.TOOLTIP_NEXT_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);

	// NOW ARRANGE ALL OUR REGIONS
	borderPane = new BorderPane();
	borderPane.setTop(topPane);
	borderPane.setCenter(slideShowImageView);
	borderPane.setBottom(bottomPane);

	// NOW SETUP THE BUTTON HANDLERS
	previousButton.setOnAction(e -> {
	    slides.previous();
	    reloadSlideShowImageView();
	    reloadCaption();
	});
	nextButton.setOnAction(e -> {
	    slides.next();
	    reloadSlideShowImageView();
	    reloadCaption();
	});

	// NOW PUT STUFF IN THE STAGE'S SCENE
	Scene scene = new Scene(borderPane, 1000, 700);
	setScene(scene);
	this.showAndWait();
    }

    // HELPER METHOD
    private void reloadSlideShowImageView() {
	try {
	    Slide slide = slides.getSelectedSlide();
	    if (slide == null) {
		slides.setSelectedSlide(slides.getSlides().get(0));
	    }
	    slide = slides.getSelectedSlide();
	    String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	    File file = new File(imagePath);
	    
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    slideShowImageView.setImage(slideImage);

	    // AND RESIZE IT
	    double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    slideShowImageView.setFitWidth(scaledWidth);
	    slideShowImageView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // CANNOT SHOW A SLIDE SHOW WITHOUT ANY IMAGES
	    parentView.getErrorHandler().processError(LanguagePropertyType.ERROR_NO_SLIDESHOW_IMAGES);
	}
    }
    
    public void startWebSlideShow() throws FileNotFoundException, IOException {
        String title = slides.getTitle();
        
        //Need to Create JSON File for accessing particular slide show
        String jsonFilePath = "base/jsonslides.json";
        
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        JsonArray slidesJsonArray = makeSlidesJsonArray(slides.getSlides());
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                        .add(JSON_TITLE, title)
                                        .add(JSON_SLIDES, slidesJsonArray)
                                        .build();
        jsonWriter.writeObject(courseJsonObject);
        
                
        //Copying HTML, CSS, JS
        //Creating the new Files for the Current Slide Show
        new File("sites/"+ title).mkdir();
        new File("sites/" + title + "/css").mkdir();
        new File("sites/" + title + "/js").mkdir();
        
        
        //Copying HTML
        Path source = Paths.get("base/index.html");
        Path to = Paths.get("sites/" + title + "/index.html");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        //Copying JSON
        source = Paths.get("base/jsonslides.json");
        to = Paths.get("sites/" + title + "/js/jsonslides.json");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        //Copying CSS
        source = Paths.get("base/css/slideshow_style.css");
        to = Paths.get("sites/" + title + "/css/slideshow_style.css");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        //Copying JS
        source = Paths.get("base/js/Slideshow.js");
        to = Paths.get("sites/" + title + "/js/Slideshow.js");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        //Image Directory for the Current Slide Show
        new File("sites/" + title + "/img").mkdir();
        
        //Copy Icons
        new File("sites/" + title + "/icons").mkdir();
        
        source = Paths.get("base/img/Previous.png");
        to = Paths.get("sites/" + title + "/icons/Previous.png");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        source = Paths.get("base/img/Next.png");
        to = Paths.get("sites/" + title + "/icons/Next.png");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        source = Paths.get("base/img/play.png");
        to = Paths.get("sites/" + title + "/icons/play.png");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        source = Paths.get("base/img/pause.png");
        to = Paths.get("sites/" + title + "/icons/pause.png");
        Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
        
        ObservableList<Slide> slideShow = slides.getSlides(); 
        for(Slide s: slideShow){
            source = Paths.get(s.getImagePath() + "/" + s.getImageFileName());
            to = Paths.get("sites/" + title + "/img/" + s.getImageFileName());
            Files.copy(source, to, StandardCopyOption.REPLACE_EXISTING);
            
            
        }
        
        //File for generating webview out of html
        File webFile = new File("sites/" + title + "/index.html");
        
        String path = webFile.getAbsolutePath().replace("./","");
        WebView slideShows = new WebView();
        slideShows.getEngine().load("file:///" + path);
        Stage stage = new Stage();
        Scene scene = new Scene(slideShows, 600, 600);
        stage.setScene(scene);
        stage.show();
        
        
        
        
    }

    private void reloadCaption() {
	Slide slide = slides.getSelectedSlide();
	captionLabel.setText(slide.getCaption());
    }
    
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }
    
    private JsonObject makeSlideJsonObject(Slide slide) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
		.add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
}
